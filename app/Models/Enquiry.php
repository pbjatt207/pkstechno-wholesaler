<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Enquiry extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = "enquiries";
    protected $guarded = [];

    protected $with = ['enquiry_products', 'users'];

    public function enquiry_products(){
        return $this->hasMany(EnquiryProduct::class, 'enquiry_id', 'id');
    }

    public function users(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
