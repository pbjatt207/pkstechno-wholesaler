<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
use Illuminate\Database\Eloquent\SoftDeletes;

class EnquiryProduct extends Model
{

    use HasFactory, SoftDeletes;
    protected $table = "enquiry_products";
    protected $guarded = [];
    protected $with    = ['products'];
    protected $appends    = [];

    public function enquiry(){
        return $this->belongsTo(Enquiry::class);
    }

    public function GetWholesalerAttribute(){
        $enquiry = Enquiry::find($this->enquiry_id);
        return $enquiry;
    }

    public function products(){
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

}
