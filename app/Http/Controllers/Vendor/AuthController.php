<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Contact;
use App\Models\Plan;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Session;
use Keygen;

class AuthController extends Controller
{
    public function __construct(){

    }
    public function index(){


        if(Auth::guard('vendor')->id()){
            return redirect(route('vendor_dashboard'));
        }
        return view('vendor.login');
    }

    public function login(Request $request){

        if(Auth::guard('vendor')->id()){
            return redirect(route('vendor_dashboard'));
        }

        $request->validate([
            'email' => 'required|email|',
            'password' => 'required|string|min:6',
        ]);

        $is_exists = User::where('user_role', 'vendor')->where('email', $request->email)->first();
        if($is_exists->is_blocked){
            return back()->with('error', 'Your Account is temporary blocked!');
        }

        $remember = $request->remember;
        $userData = array(
            'email'     => $request->email,
            'user_role' => 'vendor',
            'password'  => $request->password
        );
        $info = Auth::guard('vendor')->attempt($userData, $remember);

        if($info){
            $info = Auth::guard('vendor')->user();
            $info->createToken('messageApp')->accessToken;
            return redirect(route('vendor_dashboard'));
        }

        return back()->with('error', "Invalid Email or Password");
    }

    public function logout () {
        //logout user
        Auth::guard('vendor')->logout();
        // redirect to homepage
        return redirect(url('vendor'));
    }

    public function forgot(Request $request){
        if(!empty($request->email)){
            $user = User::select('id', 'mobile', 'email', 'name')->where('email', $request->email)->where('user_role', 'dealer')->orderBy('id', 'desc')->first();

            if($user && $user->id){
                $user->reset_token = $token = Keygen\Keygen::numeric(32)->generate();
                $user->save();

                $company = Company::find(1);
                $companyName = $company->site_title;
                $subject     = 'Reset Password link email from ' . $companyName;

//                $from   = $company->user->email;
                $from   = env('mail_from_address');
                $to     = $request->email;
                $name   = $user->name;
                $url    = route('dealer_reset_password', $token);

                $data        = compact('companyName', 'subject', 'from', 'to', 'name', 'url');
                Mail::send('emails.reset_link', $data, function($message) use ($data) {
                    $message->to($data['to'], $data['name'])
                        ->subject($data['subject'])
                        ->from($data['from'], $data['companyName'])
                        ->replyTo($data['from'], $data['companyName']);
                });
                return redirect(route('dealer_forgot'))->with('success', 'Reset link has been sent on your registered email!');

            } else {
                return redirect(route('dealer_forgot'))->with('error', "Email doesn't exists" );
            }
        }
        return view('dealer.forgot_password');
    }

    public function reset($key){
        $user = User::select('id')->where('reset_token', $key)->first();
        if(!$user && empty($user->id)){
            return redirect(route('dealer_forgot'));
        }
        return view('dealer.reset_password', compact('key'));
    }
    public function reset_password($key, Request $request){

        $request->validate([
            'password' => 'required|string|confirmed',
            'password_confirmation' => 'required|string',
        ]);

        $password = $request->password;
        $user = User::select('id')->where('reset_token', $key)->first();

        if(!empty($user) && !empty($user->id)){
            $user->password = Hash::make($password);
            $user->save();
            $msg = "Password reset successfully!";
            return redirect(url('msg-dealer'))->with('flash_message_success', $msg);
        }
        return back()->with('flash_message_success', "Token invalid or expired!");

    }

    public function change_password(Request $request)
    {

        $data['menu'] = "change_password";

//        $valid = Validator::make($request->all(), [
//            'password' => ['required', 'string', 'min:8', 'confirmed'],
//            'current_password' => ['required', 'string', 'min:8']
//        ]);


        if($request->isMethod("POST")){

            $request->validate([
                'password' => 'required|string|confirmed',
                'current_password' => 'required|string',
            ]);


            if (Hash::check($request->current_password, Auth::guard('vendor')->user()->password)) {
                $password = $request->password;
                $user = Auth::guard('vendor')->user();
                $user->password = Hash::make($password);
//                dd($user);
                $user->save();

                $msg = "Password changed successfully!";
                return back()->with('success', $msg);

            }else{
                return back()->with('error', 'Invalid current password!');
            }
        }

        return view('ve


        ndor.change_password', compact('data'));

    }

    public function send_otp(){

        $user = Auth::guard('dealer')->user();
        if($user){

            $otp = rand(000000, 999999);
            $user->otp = $otp;
            $user->save();
            return back()->with('success', 'OTP has been sent successfully');
        }

        return back();

    }
}
