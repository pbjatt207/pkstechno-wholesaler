<?php

namespace App\Http\Controllers\dealer;

use App\Http\Controllers\Controller;
use App\Models\DealerCommission;
use App\Models\Payment_history;
use App\Models\Plan;
use App\Models\User_document;
use App\Models\Userplan;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\DocBlock\Tags\Uses;
use Razorpay\Api\Api;
use Validator;

class BusinessController extends Controller
{
    public function __construct(){

        $this->pageTitle 	= "Businesses";
        $this->pageInfo 	= "businesses";

        $this->homeLink 	= "dealer/";
        $this->user_role 	= "business";
        $this->pageLink 	= $this->homeLink."businesses";

        $this->data['menu'] = $this->pageInfo;
        $this->data['title'] = $this->pageTitle;
        $this->data['pageLink'] = $this->pageLink;
    }

    public function index(){
        $data = $this->data;

        $businesses = User::where('user_role', $this->user_role )->get();

        return view($this->pageLink, compact('data','businesses'));
    }

    public function grid(Request $request){

        $user_id = Auth::guard('dealer')->id();
        $columns = array(
            0 =>'id',
            1 =>'name',
            2=> 'mobile',
            3=> 'created_at',
            4=> 'is_verified',
        );

        $totalData = User::where('user_role', $this->user_role)->Where('referred_by', $user_id)->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = User::Where('user_role', $this->user_role)
                ->Where('referred_by', $user_id)
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $posts =  User::where('id','LIKE',"%{$search}%")
                ->Where('user_role', $this->user_role)
                ->Where('referred_by', $user_id)
                ->orWhere('name', 'LIKE',"%{$search}%")
                ->orWhere('mobile', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = User::where('id','LIKE',"%{$search}%")
                ->orWhere('name', 'LIKE',"%{$search}%")
                ->Where('referred_by', $user_id)
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
//                $show =  route('posts.show',$post->id);
//                $edit =  route('posts.edit',$post->id);
                $show =  '#';
                $edit =  route('edit_business', $post->id);
                $action = '<a title="Update Profile" class="btn-small btn-icon orange" href="'.$edit.'"><i class="material-icons">edit</i></a> ';
//                $action .= '<a title="View Profile" class="btn-small btn-icon btn-light-purple" href="'.$show.'"><i class="material-icons">remove_red_eye</i></a> ';
                $action .= '<a title="Activated" class="btn btn-small green btn-icon "><i class="material-icons">check</i></a>';

                $title = '<a href="'.route('dealer_business_details',$post->id).'">'.ucfirst($post->name).'</a>';
                $nestedData['id'] = $post->id;
                $nestedData['name'] = $title;
                $nestedData['mobile'] = $post->mobile;
                $nestedData['created_at'] = date('d/m/Y',strtotime($post->created_at));
                $nestedData['is_verified'] = $post->mobile;
                $nestedData['options'] = $action;
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function create(){

        $data = $this->data;
        $id = '';
        $dealers = User::where('user_role', 'dealer' )->get();
        return view('dealer.business_add_edit', compact('id','data','dealers'));
    }

    public function store(Request $request)
    {
        $user_id = Auth::guard('dealer')->id();
        $doc = array();

        $request->validate([
            'name' => 'required|min:3',
            'email' => 'required|unique:users',
            'mobile' => 'required|numeric|unique:users',
            'sender_id' => 'required|unique:users',
        ]);

        $password = "123456";
        $password = Hash::make($password);
        $sender_id = $request->sender_id;
        $is_sender_available = User::where('sender_id', $sender_id)->first();

        if($is_sender_available){
            return back()->with('error', __('auth.sender_id_already_exists'));
        }

        $user = new User;
        $user->referred_by = $user_id;
        $user->password   = $password;
        $user->name       = $request->name;
        $user->email      = $request->email;
        $user->mobile     = $request->mobile;
        $user->user_role  = $this->user_role;
        $user->sender_id  = $sender_id;

        if ($file = $request->file('image')) {
            $optimizeImage = Image::make($file);
            $optimizePath = public_path().'/images/users/';
            $name = time().$file->getClientOriginalName();
            $optimizeImage->save($optimizePath.$name, 72);
            $user->image = $name;
        }

        $user->save();
        $doc = new User_document;

        if ($file = $request->file('pan_card')) {
            $optimizeImage = Image::make($file);
            $optimizePath = public_path().'/images/documents/';
            $name = time().$file->getClientOriginalName();
            $optimizeImage->save($optimizePath.$name, 72);
            $doc->pan_card = $name;
        }
        if ($file = $request->file('aadhar_front')) {
            $optimizeImage = Image::make($file);
            $optimizePath = public_path().'/images/documents/';
            $name = time().$file->getClientOriginalName();
            $optimizeImage->save($optimizePath.$name, 72);
            $doc->aadhar_front = $name;
        }
        if ($file = $request->file('aadhar_back')) {
            $optimizeImage = Image::make($file);
            $optimizePath = public_path().'/images/documents/';
            $name = time().$file->getClientOriginalName();
            $optimizeImage->save($optimizePath.$name, 72);
            $doc->aadhar_back = $name;
        }
        $doc->uid = $user->id;
        $doc->save();


        return redirect(route('dealer_business'))->with('added',  __('auth.dealer_created'));

    }

    public function edit($id){

        $data       = $this->data;
        $dealers    = User::where('user_role', 'dealer' )->get();
        $business   = User::with(['document'])
                        ->where('users.id',$id)
                        ->get()->first();

        return view('admin.business_add_edit', compact('id','data','dealers', 'business'));
    }

    public function check_sender_id(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'sender_id' => 'required|min:6|max:6'
            ]
        );

        if ($validator->fails()){
            return response()->json($validator->messages());
        }

        $is_exists = User::where('sender_id', $request->sender_id)->get()->first();

        if($is_exists){
            return response()->json(['message' =>  __('auth.sender_id_already_exists'),'status' => false]);
        }
        return response()->json(['message' =>  __('auth.available_sender_id'),'status' => true]);

    }

    public function details($id){

        $data = $this->data;

        $user = User::with(['send_messages','document','user_plans' => function($up){
//            $up->join('plans', 'plans.id','=','user_plans.plan_id')->select('plans.title','plans.amount as pAmount');
            }])
            ->where('users.user_role', 'business' )
            ->where('users.id', $id )
            ->first();

        if(!$user){
            return redirect(route('businesses'));
        }

        $referraluser = array();
        if(!empty($user->referred_by)){
            $referraluser = User::find($user->referred_by);
        }

        $plan_purchased = array();
        if(!empty($user->user_plans)){
            $plan_purchased = $user->user_plans;
            foreach ($plan_purchased as $key => $each){
                $plan = Plan::find($each->plan_id);
                $each->title = $plan->title;
            }
        }

        $plans = Plan::where('is_active', 1)->get();

        $dealer_info = Auth::guard('dealer')->user();

        $purchased = Userplan::Where('uid', $id)
            ->Where('status', 'A')
            ->sum('message');

        return view('dealer.business_details', compact('id', 'data','user','dealer_info','plan_purchased', 'purchased', 'referraluser', 'plans'));
    }


    public function purchase_plan(Request $request){

        $status     = 301;
        $msg        = "Invalid Request";
        $userId     = auth('dealer')->id();

        $validator  = Validator::make($request->all(),
            [
                'plan_id' => 'required',
            ]
        );

        if ($validator->fails()){
            return response()->json($validator->messages());
        }

        $plan_id        = $request->plan_id;
//        $plan_id        = 1;
        $business       = $request->user_id;
        $transaction_id = $request->razorpay_payment_id;
//        $transaction_id = 'pay_GKKjTsL2Yh61kg';
        $plan           = Plan::find($plan_id);

        if($plan){

            $razorpayId = env('RAZOR_KEY');
            $razorpayKey = env('RAZOR_SECRET');

            $api = new Api($razorpayId, $razorpayKey);
            $payment = $api->payment->fetch($transaction_id);

            $msg = "Payment failed, please try again";
            if($payment->status == 'captured' || $payment->status == 'authorized'){

                $amount =  ($payment->amount / 100);
                $transaction_id         = $payment->id;
                $history['uid']         = $business;
                $history['mode']        = 'Razzorpay';
                $history['amount']      = $amount;
                $history['plan_id']     = $plan_id;
                $history['purchase_by'] = $userId;
                $history['transaction_id']      = $transaction_id;
                $history['payment_captured_on'] = date("Y-m-d H:i:", $payment->created_at);

                $res = Payment_history::create($history);

                if($res){

                    // Save User Plan Records
                    $plan_status = 'A';
                    $userplan = new Userplan;
                    $userplan->uid        = $business;
                    $userplan->plan_id    = $plan_id;
                    $userplan->status     = $plan_status;
                    $userplan->message    = $plan->message; // No of messages in the plan
                    $userplan->amount     = $amount; // Amount of the plan
                    $userplan->payment_id = $res->id; // Payment History ID
                    $userplan->save();

                    // Save Dealer Commission if business invited by dealer
                    if(!empty($plan->commission_rate)){

                        $commission = new DealerCommission;
                        $commission->amount         = number_format($plan->amount * ($plan->commission_rate / 100), 2);
                        $commission->user_id        = $userId;
                        $commission->plan_id        = $plan_id;
                        $commission->user_plan_id   = $userplan->id;
                        $commission->commission_rate = $plan->commission_rate;
                        $commission->save();

                    }

                    $msg = "Message plan purchased successfully";
                    $status = 200;
                }

            }

        }

        return response()->json(['message' =>  $msg ],$status);

    }
}
