<?php

namespace App\Http\Controllers\dealer;

use App\Http\Controllers\Controller;
use App\Models\DealerCommission;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommissionController extends Controller
{
    public function __construct(){

        $this->pageTitle 	= "Commissions";
        $this->pageInfo 	= "commission";

        $this->homeLink 	= "msg-dealer/";
        $this->pageLink 	= "dealer/commissions";

        $this->data['menu'] = $this->pageInfo;
        $this->data['title'] = $this->pageTitle;
        $this->data['pageLink'] = $this->pageLink;
    }

    public function index(){
        $data = $this->data;
        return view('dealer.commission', compact('data'));
    }

    public function grid(Request $request){

        $user_id = Auth::guard('dealer')->id();
        $columns = array(
            0 =>'id',
            1 =>'user_id',
            2=> 'plan_id',
            3=> 'commission_rate',
            4=> 'amount',
            4=> 'created_at',
        );

        $totalData = DealerCommission::Where('dealer_commission.user_id', $user_id)->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = DealerCommission::Where('dealer_commission.user_id', $user_id)
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

        } else {
            $search = $request->input('search.value');

            $posts =  DealerCommission::where('id','LIKE',"%{$search}%")
                ->where('amount','LIKE',"%{$search}%")
                ->Where('dealer_commission.user_id', $user_id)
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = DealerCommission::where('dealer_commission.id','LIKE',"%{$search}%")
                ->orWhere('amount', 'LIKE',"%{$search}%")
                ->Where('user_id', $user_id)
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
//                echo "<pre>";
//                print_r($post);
//                echo "<pre>"; die;

                $nestedData['id'] = $post->id;
                $nestedData['user_id'] = $post->userplans->users->name;
                $nestedData['plan_id'] = $post->plans->title;
                $nestedData['rate'] = $post->commission_rate.'%';
                $nestedData['amount'] = '&#8377;'.$post->amount;
                $nestedData['created_at'] = date('d/m/Y',strtotime($post->created_at));
                $nestedData['is_verified'] = $post->mobile;
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function profile(){

        $id     = Auth::guard('dealer')->id();
        $this->data['menu'] = "profile";
        $data = $this->data;
        $user   = User::find($id);

        $referralBusiness = User::where('referred_by', $id)->get();
        $commission = DealerCommission::where('dealer_commission.user_id', $id)->get();

        return view('dealer.dealer_details', compact('data','user', 'referralBusiness', 'commission'));
    }
}
