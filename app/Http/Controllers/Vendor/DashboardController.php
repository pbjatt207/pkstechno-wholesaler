<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\Models\DealerCommission;
use App\Models\EnquiryProduct;
use App\Models\Product;
use App\Models\User;
use App\Models\User_document;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __construct(){

        $this->pageTitle 	= "Dashboard";
        $this->pageInfo 	= "dashboard";

        $this->homeLink 	= "vendor/";
        $this->pageLink 	= $this->homeLink."dashboard";

        $this->data['menu'] = $this->pageInfo;
        $this->data['title'] = $this->pageTitle;
        $this->data['pageLink'] = $this->pageLink;
    }

    public function index(){
        $data = $this->data;

        $user_id = Auth::guard('vendor')->id();
        $wholesaler = User::where('user_role', 'wholesaler')
            ->where('referred_by', $user_id)
            ->count();
        $last24hr_wholesaler    = User::where('user_role', 'wholesaler')
            ->where('referred_by', $user_id)
            ->where('created_at', '>', Carbon::now()->subDays(1))
            ->groupBy(\DB::raw('HOUR(created_at)'))
            ->count();

        $totalData = Product::select(DB::raw('group_concat(id) as ids'))->Where('user_id', $user_id)->first();
        $product_ids = $totalData->ids;
        $product_ids = explode(",", $product_ids);

        $last24hr_enquiry = EnquiryProduct::WhereIn('product_id', $product_ids)
            ->where('created_at', '>', Carbon::now()->subDays(1))
            ->groupBy(\DB::raw('HOUR(created_at)'))
            ->count();

        $enquiry = EnquiryProduct::WhereIn('product_id', $product_ids)
            ->count();

        $products = Product::where('user_id', $user_id)->count();

        return view($this->pageLink, compact('data', 'products','wholesaler', 'last24hr_wholesaler', 'last24hr_enquiry', 'enquiry'));
    }

    public function profile(){

        $id     = Auth::guard('vendor')->id();
        $this->data['menu'] = "profile";
        $this->data['title'] = "Profile";
        $data = $this->data;
        $user   = User::find($id);

        $referralBusiness = User::where('referred_by', $id)->get();
//        $commission = DealerCommission::where('dealer_commission.user_id', $id)->get();

        return view('vendor.vendor_details', compact('data','user', 'referralBusiness'));
    }

    public function verify(Request $request){

        $id     = Auth::guard('dealer')->id();
        $data   = $this->data;
        $user   = User::find($id);

        if($request->isMethod('post')){
            $request->validate([
                'otp' => 'required|numeric|min:6',
            ]);

            if($request->otp == $user->otp){
                $user->is_verified = 'Y';
                $user->save();
                return redirect(route('dealer_dashboard'))->with('success', 'Account has successfully verified');
            } else {
                return back()->with('error', 'Invalid OTP!');
            }
        }

        $this->data['menu'] = "verify";
        return view('dealer.verify_user', compact('data','user'));
    }

    public function update(){

        $this->data['menu'] = 'profile';
        $this->data['title'] = 'Profile';
        $data   = $this->data;
        $retailer = Auth()->guard('vendor')->user();

        return view('vendor.vendor_add_edit', compact('retailer','data'));
    }

    public function store(Request $request)
    {

        $dealer = Auth::guard('vendor')->user();
        $id     = $dealer->id;

        $request->validate([
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users,email,'.$id,
            'mobile' => 'required|numeric|unique:users,mobile,'.$id,
            'shop' => 'required',
        ]);

        $dealer->name = $request->name;
        $dealer->email = $request->email;
        $dealer->mobile = $request->mobile;
        $dealer->shop = $request->shop;
        $dealer->address = $request->address;
        $dealer->save();

        return redirect(route('vendor_profile'))->with('success', 'Profile has been updated successfully');

    }
}
