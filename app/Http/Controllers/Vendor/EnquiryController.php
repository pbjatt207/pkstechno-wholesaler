<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\Models\DealerCommission;
use App\Models\Enquiry;
use App\Models\EnquiryProduct;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EnquiryController extends Controller
{
    public function __construct(){

        $this->pageTitle 	= "Enquiry";
        $this->pageInfo 	= "enquiry";

        $this->homeLink 	= "vendor/";
        $this->pageLink 	= $this->homeLink."enquiry";

        $this->data['menu'] = $this->pageInfo;
        $this->data['title'] = $this->pageTitle;
        $this->data['pageLink'] = $this->pageLink;
    }

    public function index(){
        $data = $this->data;
        $user_id = Auth::guard('vendor')->id();
//        $totalData = Enquiry::Where('user_id', $user_id)->get();
//        dd($totalData);


//        $totalData = Product::select(DB::raw('group_concat(id) as ids'))->Where('user_id', $user_id)->first();
//        $enquiry_ids = $totalData->ids;
//        $enquiry_ids = explode(",", $enquiry_ids);
//
//        $totalData = EnquiryProduct::WhereIn('product_id', $enquiry_ids)->get();
//        dd($totalData);
        return view($this->pageLink, compact('data'));
    }

    public function grid(Request $request){

        $user_id = Auth::guard('vendor')->id();
        $columns = array(
            0 =>'id',
            1 =>'enquiry_id',
            2=> 'product_id',
            3=> 'quantity',
            4=> 'amount',
            4=> 'created_at',
        );

        $totalData = Product::select(DB::raw('group_concat(id) as ids'))->Where('user_id', $user_id)->first();
        $enquiry_ids = $totalData->ids;
        $enquiry_ids = explode(",", $enquiry_ids);

        $totalData = EnquiryProduct::WhereIn('product_id', $enquiry_ids)->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = EnquiryProduct::WhereIn('product_id', $enquiry_ids)
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

        } else {
            $search = $request->input('search.value');

            $posts =  EnquiryProduct::WhereIn('product_id', $enquiry_ids)
                ->where('id','LIKE',"%{$search}%")
//                ->where('name','LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = EnquiryProduct::WhereIn('product_id', $enquiry_ids)
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {

            foreach ($posts as $post)
            {

                $enquiry = Enquiry::find($post->enquiry_id, ['user_id']);
                $wholesaler = User::find($enquiry->user_id, ['name']);
                $nestedData['id'] = $post->id;
                $nestedData['enquiry'] = "#".sprintf("%06d", $post->enquiry_id);
                $nestedData['wholesaler'] = '<a href="'.route('vendor_wholesaler_details', $enquiry->user_id).'">'.$wholesaler->name.'</a>';
                $nestedData['product'] = $post->products->name;
                $nestedData['amount'] = "&#8377;".$post->amount;
                $nestedData['quantity'] = $post->quantity;
                $nestedData['created_at'] = date('d/m/Y h:i A',strtotime($post->created_at));
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function create(){

        $user   = Auth::guard('wholesaler')->user();
        $data = $this->data;
        $products = Product::where('user_id', $user->referred_by)
            ->where('is_active', 1)->get();

        return view('wholesaler.send_enquiry', compact('data','user', 'products'));
    }
}
