<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index(){

        if(auth()->id()){
            return redirect(route('admin_dashboard'));
        }
        return view('admin.login');
    }
    public function login(Request $request){

        $request->validate([
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);

        $remember = $request->remember;
        $userData = array(
            'email'     => $request->email,
            'user_role' => 'admin',
            'password'  => $request->password
        );
        $info = Auth::guard('web')->attempt($userData, $remember);

        if($info){
            $info = Auth::guard('web')->user();
            $info->createToken('messageApp')->accessToken;
            return redirect(url('admin/dashboard'));
        }

        return back();
    }
    public function logout () {
        //logout user
        auth()->logout();
        // redirect to homepage
        return redirect(url('admin'));
    }

    public function forgot(){


        return view('admin.forgot_password');
    }
}
