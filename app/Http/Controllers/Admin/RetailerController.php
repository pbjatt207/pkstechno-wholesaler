<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Plan;
use App\Models\User_document;
use App\Models\Userplan;
use App\Models\UserSenderId;
use Illuminate\Http\Request;
use App\Models\User;
use Image;
use Illuminate\Support\Facades\Hash;
use Validator;

class RetailerController extends Controller
{
    public function __construct(){

        $this->pageTitle 	= "Retailers";
        $this->pageInfo 	= "retailers";

        $this->homeLink 	= "admin/";
        $this->user_role 	= "retailer";
        $this->pageLink 	= $this->homeLink."retailers";

        $this->data['menu'] = $this->pageInfo;
        $this->data['title'] = $this->pageTitle;
        $this->data['pageLink'] = $this->pageLink;
    }

    public function index(){
        $data = $this->data;

        $businesses = User::where('user_role', $this->user_role )->get();

        return view($this->pageLink, compact('data','businesses'));
    }

    public function grid(Request $request){

        $columns = array(
            0 =>'id',
            1 =>'name',
            2=> 'mobile',
            3=> 'email',
            4=> 'created_at',
            5=> 'is_active',
        );

        $totalData = User::where('user_role', $this->user_role)->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = User::Where('user_role', $this->user_role)
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $posts =  User::where('id','LIKE',"%{$search}%")
                ->Where('user_role', $this->user_role)
                ->orWhere('name', 'LIKE',"%{$search}%")
                ->orWhere('mobile', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = User::where('id','LIKE',"%{$search}%")
                ->orWhere('name', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $edit =  route('admin_retailer_edit', $post->id);
                $action = '<a title="Update Profile" class="btn-small btn-icon orange" href="'.$edit.'"><i class="material-icons">edit</i></a> ';
                if($post->is_block){
                    $btitle = "Unblock";
                    $bcolcor = "green";
                    $bclass = "check";
                } else {
                    $btitle = "Block";
                    $bcolcor = "red";
                    $bclass = "close";
                }
                $action .= '<a title="'.$btitle.'" class="btn-small btn-icon '.$bcolcor.'"><i class="material-icons">'.$bclass.'</i></a>';
//                $action .= '<a title="View Profile" class="btn-small btn-icon btn-light-purple" href="'.$show.'"><i class="material-icons">remove_red_eye</i></a> ';
//                $action .= '<a title="Activated" class="btn btn-small green btn-icon "><i class="material-icons">check</i></a>';
                $verified = '<span class="chip lighten-1 red white-text">No</span>';
                if($post->is_active){
                    $verified = '<span class="chip green lighten-1 white-text">Yes</span>';
                }
                $title = '<a href="'.route('admin_retailer_details',$post->id).'">'.ucfirst($post->name).'</a>';
                $nestedData['id'] = $post->id;
                $nestedData['name'] = $title;
                $nestedData['mobile'] = $post->mobile;
                $nestedData['email'] = $post->email;
                $nestedData['created_at'] = date('d/m/Y h:i A',strtotime($post->created_at));
                $nestedData['is_verified'] = $verified;
                $nestedData['options'] = $action;
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function create(){

        $data = $this->data;
        $id = '';
        $dealers = User::where('user_role', 'dealer' )->get();
        return view('admin.retailer_add_edit', compact('id','data','dealers'));
    }

    public function store(Request $request, $id = null)
    {
        $request->validate([
            'name' => 'required|min:3',
            'email' => 'required|unique:users,email,'.$id,
            'mobile' => 'required|numeric|unique:users,mobile,'.$id,
            'shop' => 'required',
        ]);

        if($id){
            $user = User::find($id);
        } else {

            $user = new User;
            $password = "123456";
            $password = Hash::make($password);
            $user->password = $password;
        }

        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->mobile   = $request->mobile;
        $user->user_role  = $this->user_role;
        $user->shop     = $request->shop;
        $user->address  = $request->address;
        $user->is_active= 1;
        $user->save();

        return redirect(route('admin_retailer'))->with('added',  trans('auth/admin_retailer_added'));

    }

    public function edit($id){

        $data       = $this->data;
        $dealers    = User::where('user_role', 'dealer' )->get();
        $retailer   = User::where('users.id',$id)->first();

        return view('admin.retailer_add_edit', compact('id','data','dealers', 'retailer'));
    }

    public function details($id){

        $data = $this->data;
        $user = User::where('users.user_role', $this->user_role )
            ->where('users.id', $id )
            ->first();

        if(!$user){
            return redirect(route('admin_retailer'));
        }
//        dd($user->user_plans);
        $referraluser = array();
        if(!empty($user->referred_by)){
            $referraluser = User::find($user->referred_by);
        }

        $plan_purchased = array();
        if(!empty($user->user_plans)){
            $plan_purchased = $user->user_plans;
            foreach ($plan_purchased as $key => $each){
                $plan = Plan::find($each->plan_id);
                $each->title = $plan->title;
            }
        }

        $purchased = "";

        return view('admin.retailer_details', compact('data','user','plan_purchased', 'purchased', 'referraluser'));
    }
}
