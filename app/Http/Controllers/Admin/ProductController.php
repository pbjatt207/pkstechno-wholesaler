<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DealerCommission;
use App\Models\Payment_history;
use App\Models\Product;
use App\Models\User_document;
use App\Models\Userplan;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\DocBlock\Tags\Uses;
use Razorpay\Api\Api;
use Validator;

class ProductController extends Controller
{
    public function __construct(){

        $this->pageTitle 	= "Products";
        $this->pageInfo 	= "products";

        $this->homeLink 	= "admin/";
        $this->pageLink 	= $this->homeLink."products";

        $this->data['menu'] = $this->pageInfo;
        $this->data['title'] = $this->pageTitle;
        $this->data['pageLink'] = $this->pageLink;
    }

    public function index(){
        $data = $this->data;
        return view($this->pageLink, compact('data'));
    }

    public function grid(Request $request){

        $columns = array(
            0 =>'id',
            1 =>'name',
            2=> 'price',
            3=> 'amount',
            4=> 'created_at',
        );

        $totalData = Product::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $products = Product::offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $products =  Product::where('id','LIKE',"%{$search}%")
                ->where('name','LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Product::where('id','LIKE',"%{$search}%")
                ->orWhere('name', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();
        if(!empty($products))
        {
            foreach ($products as $rec)
            {
//                $show =  route('posts.show',$post->id);
//                $edit =  route('posts.edit',$post->id);
                $show =  '#';
//                $edit =  route('vendor_product_edit', $rec->id);
//                if($rec->is_active){
//                    $btitle = "Deactivate";
//                    $bcolcor = "green";
//                    $btype = "deactivte";
//                    $bclass = "check";
//                    $status = '<a class="red-text">Blocked</a>';
//                } else {
//                    $btitle = "Activate";
//                    $bcolcor = "red";
//                    $btype = "activate";
//                    $bclass = "close";
//                }
//                $action = '<a title="Update Profile" class="btn-small btn-icon orange" href="'.$edit.'"><i class="material-icons">edit</i></a> ';
//                $action .= '<a data-type="'.$btype.'" data-id="'.$rec->id.'" title="'.$btitle.'" class="btn-small btn-icon updateStatus '.$bcolcor.'"><i class="material-icons">'.$bclass.'</i></a> ';
//                $action .= '<a title="View Profile" class="btn-small btn-icon btn-light-purple" href="'.$show.'"><i class="material-icons">remove_red_eye</i></a> ';
//                $action .= '<a title="Activated" class="btn btn-small green btn-icon "><i class="material-icons">check</i></a> ';
//                $action .= '<a title="Delete" class="btn btn-small red btn-icon delete" data-id="'.$rec->id.'"><i class="material-icons">delete</i></a>';
                $image = asset('assets/images/cards/watch-2.png');
                $img_path = public_path().'/images/products/'.$rec->image;

                if(!empty($rec->image) && file_exists($img_path)){
                    $image = asset('images/products/'.$rec->image);
                }
                $img = '<img src="'.$image.'" width="80px">';
                $title = '<a href="">'.ucfirst($rec->name).'</a>';
                $nestedData['id'] = $rec->id;
                $nestedData['name'] = $title;
                $nestedData['image'] = $img;
                $nestedData['vendor'] = $rec->vendor->name;
                $nestedData['price'] = "&#8377;".$rec->price;
                $nestedData['amount'] = "&#8377;".$rec->amount;
                $nestedData['created_at'] = date('d/m/Y h:i A',strtotime($rec->created_at));
                $nestedData['options'] = "";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function create(){

        $data = $this->data;
        $id = '';
        return view('vendor.product_add_edit', compact('id','data'));
    }

    public function store(Request $request, $id = null)
    {
        $user_id = Auth::guard('vendor')->id();
        $doc = array();

        $request->validate([
            'name' => 'required|min:3',
            'description' => 'required',
            'amount' => 'required|numeric'
        ]);
        if(!empty($id)){
            $product = Product::find($id);
        } else {
            $product = new Product;
        }
        $product->user_id   = $user_id;
        $product->name      = $request->name;
        $product->price     = $request->price;
        $product->amount    = $request->amount;
        $product->discount  = $request->discount;
        $product->discount_value   = $request->discount_value;
        $product->description      = $request->description;


        if ($file = $request->file('image')) {
            $optimizeImage = Image::make($file);
            $optimizePath = public_path().'/images/products/';
            $name = time().$file->getClientOriginalName();
            $optimizeImage->save($optimizePath.$name, 72);
            $product->image = $name;
//            $thumbnail_path = $optimizePath.'thumb/';
//            $img = Image::make($request->file('image')->getRealPath());
//            Image::make($img)->resize(320, 320)->insert($thumbnail_path.$name);

        }

        $product->save();
        return redirect(route('vendor_product'))->with('added',  __('auth.product_added'));

    }

    public function edit($id){

        $data       = $this->data;
        $info       = Product::where('user_id', Auth::guard('vendor')->id())->where('id', $id)->first();


        return view('vendor.product_add_edit', compact('id','data', 'info'));
    }

    public function check_sender_id(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'sender_id' => 'required|min:6|max:6'
            ]
        );

        if ($validator->fails()){
            return response()->json($validator->messages());
        }

        $is_exists = User::where('sender_id', $request->sender_id)->get()->first();

        if($is_exists){
            return response()->json(['message' =>  __('auth.sender_id_already_exists'),'status' => false]);
        }
        return response()->json(['message' =>  __('auth.available_sender_id'),'status' => true]);

    }

    public function details($id){

        $data = $this->data;

        $user = User::with(['send_messages','document','user_plans' => function($up){
//            $up->join('plans', 'plans.id','=','user_plans.plan_id')->select('plans.title','plans.amount as pAmount');
            }])
            ->where('users.user_role', 'business' )
            ->where('users.id', $id )
            ->first();

        if(!$user){
            return redirect(route('businesses'));
        }

        $referraluser = array();
        if(!empty($user->referred_by)){
            $referraluser = User::find($user->referred_by);
        }

        $plan_purchased = array();
        if(!empty($user->user_plans)){
            $plan_purchased = $user->user_plans;
            foreach ($plan_purchased as $key => $each){
                $plan = Plan::find($each->plan_id);
                $each->title = $plan->title;
            }
        }

        $plans = Plan::where('is_active', 1)->get();

        $dealer_info = Auth::guard('dealer')->user();

        $purchased = Userplan::Where('uid', $id)
            ->Where('status', 'A')
            ->sum('message');

        return view('dealer.business_details', compact('id', 'data','user','dealer_info','plan_purchased', 'purchased', 'referraluser', 'plans'));
    }


    public function purchase_plan(Request $request){

        $status     = 301;
        $msg        = "Invalid Request";
        $userId     = auth('dealer')->id();

        $validator  = Validator::make($request->all(),
            [
                'plan_id' => 'required',
            ]
        );

        if ($validator->fails()){
            return response()->json($validator->messages());
        }

        $plan_id        = $request->plan_id;
//        $plan_id        = 1;
        $business       = $request->user_id;
        $transaction_id = $request->razorpay_payment_id;
//        $transaction_id = 'pay_GKKjTsL2Yh61kg';
        $plan           = Plan::find($plan_id);

        if($plan){

            $razorpayId = env('RAZOR_KEY');
            $razorpayKey = env('RAZOR_SECRET');

            $api = new Api($razorpayId, $razorpayKey);
            $payment = $api->payment->fetch($transaction_id);

            $msg = "Payment failed, please try again";
            if($payment->status == 'captured' || $payment->status == 'authorized'){

                $amount =  ($payment->amount / 100);
                $transaction_id         = $payment->id;
                $history['uid']         = $business;
                $history['mode']        = 'Razzorpay';
                $history['amount']      = $amount;
                $history['plan_id']     = $plan_id;
                $history['purchase_by'] = $userId;
                $history['transaction_id']      = $transaction_id;
                $history['payment_captured_on'] = date("Y-m-d H:i:", $payment->created_at);

                $res = Payment_history::create($history);

                if($res){

                    // Save User Plan Records
                    $plan_status = 'A';
                    $userplan = new Userplan;
                    $userplan->uid        = $business;
                    $userplan->plan_id    = $plan_id;
                    $userplan->status     = $plan_status;
                    $userplan->message    = $plan->message; // No of messages in the plan
                    $userplan->amount     = $amount; // Amount of the plan
                    $userplan->payment_id = $res->id; // Payment History ID
                    $userplan->save();

                    // Save Dealer Commission if business invited by dealer
                    if(!empty($plan->commission_rate)){

                        $commission = new DealerCommission;
                        $commission->amount         = number_format($plan->amount * ($plan->commission_rate / 100), 2);
                        $commission->user_id        = $userId;
                        $commission->plan_id        = $plan_id;
                        $commission->user_plan_id   = $userplan->id;
                        $commission->commission_rate = $plan->commission_rate;
                        $commission->save();

                    }

                    $msg = "Message plan purchased successfully";
                    $status = 200;
                }

            }

        }

        return response()->json(['message' =>  $msg ],$status);

    }

    public function delete(Request $request){

        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json($validator->messages());
        }

        $product = Product::where('user_id', Auth::guard('vendor')->id())->where('id', $request->id)->first();
        $product->delete();

        return response()->json(['msg'=> "Product has been deleted"], 200);
    }

    public function change_status(Request $request){

        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json($validator->messages());
        }

        $product = Product::where('user_id', Auth::guard('vendor')->id())->where('id', $request->id)->first();
        if($product->is_active){
            $product->is_active = 0;
            $msg = "Product has been deactivated successfully";

        } else {
            $product->is_active = 1;
            $msg = "Product has been activated successfully";
        }
        $product->save();

        return response()->json(['msg'=> $msg], 200);

    }
}
