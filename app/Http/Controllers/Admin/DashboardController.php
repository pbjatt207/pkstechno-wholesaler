<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\DealerCommission;
use App\Models\Payment_history;
use App\Models\Product;
use App\Models\User;
use App\Models\Userplan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Charts\RevenueChart;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __construct(){

        $this->pageTitle 	= "Dashboard";
        $this->pageInfo 	= "dashboard";

        $this->homeLink 	= "admin/";
        $this->pageLink 	= $this->homeLink."dashboard";

        $this->data['menu'] = $this->pageInfo;
        $this->data['title'] = $this->pageTitle;
        $this->data['pageLink'] = $this->pageLink;
    }

    public function index(){

//        if(!Auth::id()){
//            return redirect(url('admin'));
//        }

        $data = $this->data;
        $total_wholesaler       = User::where('user_role', 'wholesaler')->count();
        $last24hr_wholesaler    = User::where('user_role', 'wholesaler')->where('created_at', '>', Carbon::now()->subDays(1))
            ->groupBy(\DB::raw('HOUR(created_at)'))->count();
        $last24hr_vendor  = User::where('user_role', 'vendor')->where('created_at', '>', Carbon::now()->subDays(1))
            ->groupBy(\DB::raw('HOUR(created_at)'))->count();
        $total_vendor     = User::where('user_role', 'vendor')->count();
        $total_product     = Product::count();
        $last24hr_product  = Product::where('created_at', '>', Carbon::now()->subDays(1))
            ->groupBy(\DB::raw('HOUR(created_at)'))->count();

//        $last24hr_income  = Userplan::where('user_plans.status','!=', 'P')->where('created_at', '>', Carbon::now()->subDays(1))
//            ->groupBy(\DB::raw('HOUR(created_at)'))->sum('amount');
//        $total_income     = Userplan::where('user_plans.status', '!=', 'P')->sum('amount');
//
//        $last5_commission_records = DealerCommission::orderBy('id', 'desc')->take(5)->get();
//        $payment_history = Payment_history::orderBy('id', 'desc')->take(5)->get();
        $last24hr_income = $total_income = $last5_commission_records = $payment_history = 0;

        return view('admin.dashboard', compact('data', 'total_wholesaler','last24hr_product','total_product','total_vendor', 'last24hr_vendor','last5_commission_records', 'last24hr_wholesaler', 'last24hr_income', 'total_income'));
    }
}
