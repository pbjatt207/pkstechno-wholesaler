<?php

namespace App\Http\Controllers\Wholesaler;

use App\Http\Controllers\Controller;
use App\Models\DealerCommission;
use App\Models\Enquiry;
use App\Models\EnquiryProduct;
use App\Models\Payment_history;
use App\Models\Product;
use App\Models\User_document;
use App\Models\Userplan;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\DocBlock\Tags\Uses;
use Razorpay\Api\Api;
use Validator;

class ProductController extends Controller
{
    public function __construct(){

        $this->pageTitle 	= "Products";
        $this->pageInfo 	= "products";

        $this->homeLink 	= "wholesaler/";
        $this->pageLink 	= $this->homeLink."products";

        $this->data['menu'] = $this->pageInfo;
        $this->data['title'] = $this->pageTitle;
        $this->data['pageLink'] = $this->pageLink;
    }

    public function index(Request $request){

        $data = $this->data;
        $user = Auth::guard('wholesaler')->user();
        $user_id    = $user->id;
        $vendor_id  = $user->referred_by;

        if($request->isMethod('post')){
            $products = $request->product;
            $quantity = $request->quantity;
            if(!empty($products) && !empty($quantity)){
                $enquiry = new Enquiry;
                $enquiry->user_id = $user_id;
                $enquiry->save();
                foreach ($products as $key => $product_id){
                    if(!empty($quantity[$product_id])){
                        $eproduct = new EnquiryProduct;
                        $eproduct->enquiry_id = $enquiry->id;
                        $eproduct->product_id = $product_id;
                        $eproduct->quantity   = $quantity[$product_id];
                        $eproduct->amount     = Product::find($product_id)->amount;
                        $eproduct->save();
                    }

                    return back()->with('success', 'Enquiry has been sent successfully to the vendor');
                }
            }
        }
        $products = Product::where('user_id', $vendor_id)
            ->where('is_active', 1)->get();
        return view($this->pageLink, compact('data', 'products'));
    }

    public function grid(Request $request){

        $user = Auth::guard('wholesaler')->user();
        $user_id    = $user->id;
        $vendor_id  = $user->referred_by;
        $columns = array(
            0 =>'id',
            1 =>'name',
            2=> 'name',
            3=> 'amount',
        );

        $totalData = Product::where('user_id', $vendor_id)
            ->where('is_active', 1)->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $products = Product::Where('user_id', $vendor_id)
                ->where('is_active', 1)
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $products =  Product::where('id','LIKE',"%{$search}%")
                ->Where('user_id', $vendor_id)
                ->where('is_active', 1)
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Product::where('id','LIKE',"%{$search}%")
                ->orWhere('name', 'LIKE',"%{$search}%")
                ->Where('user_id', $vendor_id)
                ->where('is_active', 1)
                ->count();
        }

        $data = array();
        if(!empty($products))
        {
            foreach ($products as $rec)
            {

                $input = '<input type="number" class="" min="0" name="quantity['.$rec->id.'][]" val="0">';
                $action = '<a title="Add to Cart" class="btn-small btn-icon orange" data-id="'.$rec->id.'"><i class="material-icons">add</i> Add to Cart</a> ';
                $title = '<a href="'.route('wholesaler_product_details',$rec->id).'">'.ucfirst($rec->name).'</a>';
                $nestedData['id'] = $rec->id;
                $nestedData['image'] = $title;
                $nestedData['name'] = $title;
                $nestedData['amount'] = "&#8377;".$rec->amount;
                $nestedData['input'] = $input;
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function create(){

        $data = $this->data;
        $id = '';
        return view('vendor.product_add_edit', compact('id','data'));
    }

    public function store(Request $request)
    {
        $user_id = Auth::guard('vendor')->id();
        $doc = array();

        $request->validate([
            'name' => 'required|min:3',
            'description' => 'required',
            'amount' => 'required|numeric'
        ]);

        $product = new Product;
        $product->user_id   = $user_id;
        $product->name      = $request->name;
        $product->price     = $request->price;
        $product->amount    = $request->amount;
        $product->discount  = $request->discount;
        $product->discount_value   = $request->discount_value;
        $product->description      = $request->description;


        if ($file = $request->file('image')) {
            $optimizeImage = Image::make($file);
            $optimizePath = public_path().'/images/product/';
            $name = time().$file->getClientOriginalName();
            $optimizeImage->save($optimizePath.$name, 72);
            $product->image = $name;
        }

        $product->save();
        return redirect(route('vendor_product'))->with('added',  __('auth.product_added'));

    }

    public function edit($id){

        $data       = $this->data;
        $info       = Product::where('user_id', Auth::guard('vendor')->id())->where('id', $id)->first();


        return view('vendor.product_add_edit', compact('id','data', 'info'));
    }

    public function details($id){

        $data = $this->data;
        $vendor_id = Auth::guard('wholesaler')->user()->referred_by;
        $product = Product::where('user_id', $vendor_id)->where('id', $id)->first();

        return view('wholesaler.product_details', compact('id', 'product', 'data'));
    }


    public function purchase_plan(Request $request){

        $status     = 301;
        $msg        = "Invalid Request";
        $userId     = auth('dealer')->id();

        $validator  = Validator::make($request->all(),
            [
                'plan_id' => 'required',
            ]
        );

        if ($validator->fails()){
            return response()->json($validator->messages());
        }

        $plan_id        = $request->plan_id;
//        $plan_id        = 1;
        $business       = $request->user_id;
        $transaction_id = $request->razorpay_payment_id;
//        $transaction_id = 'pay_GKKjTsL2Yh61kg';
        $plan           = Plan::find($plan_id);

        if($plan){

            $razorpayId = env('RAZOR_KEY');
            $razorpayKey = env('RAZOR_SECRET');

            $api = new Api($razorpayId, $razorpayKey);
            $payment = $api->payment->fetch($transaction_id);

            $msg = "Payment failed, please try again";
            if($payment->status == 'captured' || $payment->status == 'authorized'){

                $amount =  ($payment->amount / 100);
                $transaction_id         = $payment->id;
                $history['uid']         = $business;
                $history['mode']        = 'Razzorpay';
                $history['amount']      = $amount;
                $history['plan_id']     = $plan_id;
                $history['purchase_by'] = $userId;
                $history['transaction_id']      = $transaction_id;
                $history['payment_captured_on'] = date("Y-m-d H:i:", $payment->created_at);

                $res = Payment_history::create($history);

                if($res){

                    // Save User Plan Records
                    $plan_status = 'A';
                    $userplan = new Userplan;
                    $userplan->uid        = $business;
                    $userplan->plan_id    = $plan_id;
                    $userplan->status     = $plan_status;
                    $userplan->message    = $plan->message; // No of messages in the plan
                    $userplan->amount     = $amount; // Amount of the plan
                    $userplan->payment_id = $res->id; // Payment History ID
                    $userplan->save();

                    // Save Dealer Commission if business invited by dealer
                    if(!empty($plan->commission_rate)){

                        $commission = new DealerCommission;
                        $commission->amount         = number_format($plan->amount * ($plan->commission_rate / 100), 2);
                        $commission->user_id        = $userId;
                        $commission->plan_id        = $plan_id;
                        $commission->user_plan_id   = $userplan->id;
                        $commission->commission_rate = $plan->commission_rate;
                        $commission->save();

                    }

                    $msg = "Message plan purchased successfully";
                    $status = 200;
                }

            }

        }

        return response()->json(['message' =>  $msg ],$status);

    }

    public function delete(Request $request){

        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json($validator->messages());
        }

        $product = Product::where('user_id', Auth::guard('vendor')->id())->where('id', $request->id)->first();
        $product->delete();

        return response()->json(['msg'=> "Product has been deleted"], 200);
    }

    public function change_status(Request $request){

        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json($validator->messages());
        }

        $product = Product::where('user_id', Auth::guard('vendor')->id())->where('id', $request->id)->first();
        if($product->is_active){
            $product->is_active = 0;
            $msg = "Product has been deactivated successfully";

        } else {
            $product->is_active = 1;
            $msg = "Product has been activated successfully";
        }
        $product->save();

        return response()->json(['msg'=> $msg], 200);

    }
}
