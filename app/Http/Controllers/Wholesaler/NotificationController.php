<?php

namespace App\Http\Controllers\Dealer;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function __construct(){

        $this->pageTitle 	= "Notifications";
        $this->pageInfo 	= "notifications";

        $this->homeLink 	= "msg-dealer/";
        $this->pageLink 	= "dealer/notifications";

        $this->data['menu'] = $this->pageInfo;
        $this->data['title'] = $this->pageTitle;
        $this->data['pageLink'] = $this->pageLink;
    }

    public function index(){
        $data = $this->data;
            return view($this->pageLink, compact('data'));
    }

    public function grid(Request $request){

        $user_id = Auth::guard('dealer')->id();
        $columns = array(
            0 =>'id',
            1 =>'message',
            3=> 'created_at',
        );

        $totalData = Notification::where('send_to', $user_id)->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = Notification::where('send_to', $user_id)
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $posts =  Notification::where('send_to', $user_id)
                ->where('id','LIKE',"%{$search}%")
                ->orWhere('message', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Notification::where('send_to', $user_id)
                ->where('id','LIKE',"%{$search}%")
                ->orWhere('message', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $show =  '#';
                $edit =  route('edit_business', $post->id);
                $action = '<a title="Update Profile" class="btn-small btn-icon orange" href="'.$edit.'"><i class="material-icons">edit</i></a> ';
//                $action .= '<a title="View Profile" class="btn-small btn-icon btn-light-purple" href="'.$show.'"><i class="material-icons">remove_red_eye</i></a> ';
                $action .= '<a title="Activated" class="btn btn-small green btn-icon "><i class="material-icons">check</i></a>';

                $title = '<a href="'.route('business_details',$post->id).'">'.ucfirst($post->name).'</a>';
                $nestedData['id'] = $post->id;
                $nestedData['message'] = $post->message;
                $nestedData['created_at'] = date('d/m/Y h:i A',strtotime($post->created_at));
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }
}
