<?php

namespace App\Http\Controllers\Wholesaler;

use App\Http\Controllers\Controller;
use App\Http\Controllers\UserController;
use App\Models\DealerCommission;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\User_document;
use Hash;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Support\Facades\Validator;

class wholesalerController extends Controller
{
    public function __construct(){

        $this->pageTitle 	= "Wholesaler";
        $this->pageInfo 	= "wholesaler";

        $this->homeLink 	= "wholesaler/";
        $this->user_role 	= "wholesaler";
        $this->pageLink 	= $this->homeLink."wholesalers";

        $this->data['menu'] = $this->pageInfo;
        $this->data['title'] = $this->pageTitle;
        $this->data['pageLink'] = $this->pageLink;
    }

    public function index(){
        $data = $this->data;

//        $dealers = User::where('user_role', $this->user_role )->get();

        return view($this->pageLink, compact('data'));
    }

    public function grid(Request $request){

        $columns = array(
            0 =>'id',
            1 =>'name',
            2=> 'mobile',
            3=> 'email',
            4=> 'created_at',
            5=> 'created_at',
        );

        $totalData = User::where('user_role', $this->user_role)->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = User::Where('user_role', $this->user_role)
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $posts =  User::where('id','LIKE',"%{$search}%")
                ->Where('user_role', $this->user_role)
                ->orWhere('name', 'LIKE',"%{$search}%")
                ->orWhere('mobile', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = User::where('id','LIKE',"%{$search}%")
                ->orWhere('name', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {

                $edit =  route('vendor_wholesaler_edit', $post->id);
                $action = '<a title="Update Profile" class="btn-small btn-icon orange" href="'.$edit.'"><i class="material-icons">edit</i></a> ';
                $status = '<a class="green-text">Active</a>';
                if($post->is_blocked){
                    $btitle = "Unblock";
                    $bcolcor = "green";
                    $btype = "unblock";
                    $bclass = "Unblock";
                    $status = '<a class="red-text">Blocked</a>';
                } else {
                    $btitle = "Block";
                    $bcolcor = "red";
                    $btype = "block";
                    $bclass = "Block";
                }
                $action .= '<a data-type="'.$btype.'" data-id="'.$post->id.'" title="'.$btitle.'" class="btn-small btn-icon block '.$bcolcor.'">'.$bclass.'</a>';
                $title = '<a href="'. route('vendor_wholesaler_details', $post->id).'">'.$post->name.'</a>';
                $nestedData['id']       = $post->id;
                $nestedData['name']     = $title;
                $nestedData['mobile']   = $post->mobile;
                $nestedData['email']    = $post->email;
                $nestedData['created_at'] = date('d/m/Y h:i A',strtotime($post->created_at));
                $nestedData['status']   = $status;
                $nestedData['options']  = $action;
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function create(Request $request){

        $data   = $this->data;
        $id     = '';

        return view('vendor.wholesaler_add_edit', compact('id','data'));
    }

    public function edit(){
        $this->data['menu'] = 'profile';
        $data   = $this->data;
        $info = Auth::guard('wholesaler')->user();
        return view('wholesaler.wholesaler_add_edit', compact('data', 'info'));
    }

    public function store(Request $request)
    {

        $id = Auth::guard('wholesaler')->id();
        $request->validate([
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users,email,'.$id,
            'mobile' => 'required|numeric|unique:users,mobile,'.$id,
            'shop' => 'required',
        ]);

        $user = User::find($id);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->shop = $request->shop;
        $user->address = $request->address;
        $user->save();

        return redirect(route('wholesaler_profile'))->with('success', "Profile has been updated successfully");

    }

    public function details(){

        $this->data['menu'] = 'profile';
        $data = $this->data;
        $user = Auth::guard('wholesaler')->user();;
//        dd($user);
        $referralBusiness = User::where('referred_by',$user->id)->get();


        return view('wholesaler.wholesaler_details', compact('data','user', 'referralBusiness'));
    }

    public function block(Request $request){

        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json($validator->messages());
        }

        $wholesaler = User::where('user_role', 'wholesaler')->where('id', $request->id)->first();
        if($wholesaler->is_blocked){
            $wholesaler->is_blocked = 0;
            $msg = "Wholesaler has been Unblocked";

        } else {
            $wholesaler->is_blocked = 1;
            $msg = "Wholesaler has been blocked";
        }
        $wholesaler->save();

        return response()->json(['msg'=> $msg], 200);

    }

}
