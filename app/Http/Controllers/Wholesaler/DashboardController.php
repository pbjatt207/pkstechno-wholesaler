<?php

namespace App\Http\Controllers\Wholesaler;

use App\Http\Controllers\Controller;
use App\Models\DealerCommission;
use App\Models\Enquiry;
use App\Models\EnquiryProduct;
use App\Models\Product;
use App\Models\User;
use App\Models\User_document;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __construct(){

        $this->pageTitle 	= "Dashboard";
        $this->pageInfo 	= "dashboard";

        $this->homeLink 	= "wholesaler/";
        $this->pageLink 	= $this->homeLink."dashboard";

        $this->data['menu'] = $this->pageInfo;
        $this->data['title'] = $this->pageTitle;
        $this->data['pageLink'] = $this->pageLink;
    }

    public function index(){
        $data = $this->data;

        $user = Auth::guard('wholesaler')->user();
        $vendor_id = $user->referred_by;
        $user_id = $user->id;
        $enquiry = Enquiry::select(DB::raw('group_concat(id) as ids'))->where('user_id', $user_id)->first();
        $enquiry_products = EnquiryProduct::whereIn('enquiry_id', explode(',', $enquiry->ids))->count();
        $products = Product::where('user_id', $vendor_id)->where('is_active', 1)->count();

        return view($this->pageLink, compact('data', 'enquiry_products', 'products'));
    }

    public function verify(Request $request){

        $id     = Auth::guard('dealer')->id();
        $data   = $this->data;
        $user   = User::find($id);

        if($request->isMethod('post')){
            $request->validate([
                'otp' => 'required|numeric|min:6',
            ]);

            if($request->otp == $user->otp){
                $user->is_verified = 'Y';
                $user->save();
                return redirect(route('dealer_dashboard'))->with('success', 'Account has successfully verified');
            } else {
                return back()->with('error', 'Invalid OTP!');
            }
        }

        $this->data['menu'] = "verify";
        return view('dealer.verify_user', compact('data','user'));
    }

}
