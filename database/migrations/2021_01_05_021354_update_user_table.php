<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('mobile', 100)->after('name');
            $table->double('otp')->nullable();
            $table->string('image', 191)->nullable();
            $table->enum('user_role', ['admin', 'wholesaler', 'retailer', 'vendor'])->default('retailer');
            $table->boolean('is_active')->default(0);
            $table->boolean('is_blocked')->default(0);
            $table->text('address')->nullable();
            $table->string('shop', 191)->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
