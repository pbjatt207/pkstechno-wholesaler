<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Middleware\Vendor;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Vendor Route
Route::group(['prefix' => 'vendor','namespace' => 'App\Http\Controllers\Vendor'], function () {

    Route::get('/', 'AuthController@index')->name('vendor');
    Route::post('login', 'AuthController@login')->name('vendor_login');

    Route::group(['middleware' => 'App\Http\Middleware\Vendor', 'as' => 'vendor_' ], function () {

        Route::get('logout', 'AuthController@logout')->name('logout');
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');
        Route::get('profile', 'DashboardController@profile')->name('profile');
        Route::get( 'send_otp', 'AuthController@send_otp')->name('send_otp');
        Route::match(['get', 'post'], 'change_password', 'AuthController@change_password')->name('change_password');
        Route::match(['get', 'post'],'verify', 'DashboardController@verify')->name('verify');
        Route::get('update', 'DashboardController@update')->name('update');
        Route::post('vendor/update', 'DashboardController@store')->name('store');

        // Wholesale Route
        Route::get('wholesaler', 'wholesalerController@index')->name('wholesaler');
        Route::post('wholesaler_grid', ['uses' => 'wholesalerController@grid'])->name('wholesaler_grid');
        Route::get('wholesaler/create', ['uses' => 'wholesalerController@create'])->name('wholesaler_create');
        Route::get('wholesaler/edit/{id}', ['uses' => 'wholesalerController@edit'])->name('wholesaler_edit');
        Route::get('wholesaler/details/{id}', ['uses' => 'wholesalerController@details'])->name('wholesaler_details');
        Route::post('wholesaler/store/{id?}', ['uses' => 'wholesalerController@store'])->name('wholesaler_store');
        Route::post('wholesaler/block', ['uses' => 'wholesalerController@block'])->name('wholesaler_block');

        // Product Route
        Route::get('product', 'ProductController@index')->name('product');
        Route::post('product_grid', ['uses' => 'ProductController@grid'])->name('product_grid');
        Route::get('product/create', ['uses' => 'ProductController@create'])->name('product_create');
        Route::get('product/edit/{id}', ['uses' => 'ProductController@edit'])->name('product_edit');
        Route::get('product/details/{id}', ['uses' => 'ProductController@details'])->name('product_details');
        Route::post('product/store/{id?}', ['uses' => 'ProductController@store'])->name('product_store');
        Route::post('product/active', ['uses' => 'ProductController@change_status'])->name('product_update');
        Route::post('product/delete', ['uses' => 'ProductController@delete'])->name('product_delete');

        // Enquiry Route
        Route::get('enquiry', 'EnquiryController@index')->name('enquiry');
        Route::post('enquiry_grid', ['uses' => 'EnquiryController@grid'])->name('enquiry_grid');


        Route::get('commissions', ['uses' => 'CommissionController@index'])->name('commission');
        Route::post('commission_grid', ['uses'  => 'CommissionController@grid'])->name('commission_grid');

        // Notification Route
        Route::get('notifications', 'NotificationController@index')->name('notifications');
        Route::post('notification_grid', 'NotificationController@grid')->name('notification_grid');


    });

});
