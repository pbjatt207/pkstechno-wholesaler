<?php


use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Wholesaler Routes
Route::group(['prefix' => 'wholesaler','namespace' => 'App\Http\Controllers\Wholesaler'], function () {

    Route::get('/', 'AuthController@index')->name('wholesalerlogin');
    Route::post('login', 'AuthController@login')->name('wholesaler_login');
    Route::get('forgot', 'AuthController@forgot')->name('wholesaler_forgot');

    Route::group(['middleware' => 'wholesaler', 'as' => 'wholesaler_'], function () {

        Route::get('logout', 'AuthController@logout')->name('logout');
        Route::get('notifications', 'NotificationController@index')->name('notifications');
        Route::post('notification_grid', 'NotificationController@grid')->name('notification_grid');
        //    Route::get('forgot-password', 'AuthController@forgot')->name('admin_forgot_password');
        Route::match(['get', 'post'], 'change_password', 'AuthController@change_password')->name('change_password');
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        // Wholesaler Route
        Route::get('edit', ['uses' => 'WholesalerController@edit'])->name('edit');
        Route::post('update/store', ['uses' => 'WholesalerController@store'])->name('store');
        Route::post('wholesaler/grid', ['uses' => 'WholesalerController@grid'])->name('admin_wholesaler_grid');
        Route::get('wholesaler/create', ['uses' => 'WholesalerController@create'])->name('admin_wholesaler_create');
        Route::get('profile', ['uses' => 'WholesalerController@details'])->name('profile');

        // Enquiry Route
        Route::get('enquiry', ['uses' => 'EnquiryController@index'])->name('enquiry');
        Route::post('enquiry_grid', ['uses' => 'EnquiryController@grid'])->name('enquiry_grid');
        Route::get('enquiry/create', ['uses' => 'EnquiryController@create'])->name('enquiry_create');

        // Vendor Route
        Route::match(['get', 'post'], 'products', ['uses' => 'ProductController@index'])->name('product');
        Route::post('product_grid', ['uses' => 'ProductController@grid'])->name('product_grid');
        Route::get('product/details/{id}', ['uses' => 'ProductController@details'])->name('product_details');
        Route::get('vendor/create', ['uses' => 'vendorController@create'])->name('admin_vendor_create');
        Route::get('vendor/edit/{id}', ['uses' => 'vendorController@edit'])->name('admin_vendor_edit');
        Route::post('vendor/store/{id?}', ['uses' => 'vendorController@store'])->name('admin_vendor_store');

        // Plans Route
        Route::get('plans', ['uses' => 'PlanController@index'])->name('plans');
        Route::post('plan_grid', ['uses' => 'PlanController@grid'])->name('plan_grid');
        Route::get('plan/create', ['uses' => 'PlanController@create'])->name('create_plan');
        Route::get('plan/edit/{id}', ['uses' => 'PlanController@edit'])->name('edit_plan');
        Route::post('plan/store/{id?}', ['uses' => 'PlanController@store'])->name('store_plan');

        // Orders Route
        Route::get('orders', ['uses' => 'OrderController@index'])->name('orders');
        Route::post('order_grid', ['uses' => 'OrderController@grid'])->name('order_grid');

        // Contact Routes
        Route::get('contacts', ['uses' => 'ContactController@index'])->name('contacts');
        Route::post('contact_grid', ['uses' => 'ContactController@grid'])->name('contact_grid');

        // Sender ID Routes
        Route::get('senderid', ['uses' => 'SenderIdController@index'])->name('senderid');
        Route::post('sender_grid', ['uses' => 'SenderIdController@grid'])->name('sender_grid');
        Route::post('approve_sender', ['uses' => 'SenderIdController@approve'])->name('approve_sender');
        Route::post('reject_sender', ['uses' => 'SenderIdController@reject'])->name('reject_sender');
    });

});

// Admin Route
Route::group(['prefix' => 'admin','namespace' => 'App\Http\Controllers\Admin'], function () {

    Route::get('/', 'AuthController@index')->name('login');
    Route::post('login', 'AuthController@login')->name('admin_login');

    Route::group(['middleware' => 'auth'], function () {

        Route::get('logout', 'AuthController@logout')->name('admin_logout');
        Route::get('notifications', 'NotificationController@index')->name('notifications');
        Route::post('notification_grid', 'NotificationController@grid')->name('notification_grid');
    //    Route::get('forgot-password', 'AuthController@forgot')->name('admin_forgot_password');

        Route::get('dashboard', 'DashboardController@index')->name('admin_dashboard');
        Route::post('check_sender_id', 'BusinessController@check_sender_id')->name('check_sender');

        // Dealer Route
        Route::get('wholesaler', ['uses' => 'wholesalerController@index'])->name('admin_wholesaler');
        Route::post('wholesaler/grid', ['uses' => 'wholesalerController@grid'])->name('admin_wholesaler_grid');
        Route::get('wholesaler/create', ['uses' => 'wholesalerController@create'])->name('admin_wholesaler_create');
        Route::get('wholesaler/edit/{id}', ['uses' => 'wholesalerController@edit'])->name('admin_wholesaler_edit');
        Route::post('wholesaler/store/{id?}', ['uses' => 'wholesalerController@store'])->name('admin_wholesaler_store');
        Route::get('wholesaler/details/{id}', ['uses' => 'wholesalerController@details'])->name('admin_wholesaler_details');
        Route::post('wholesaler/block', ['uses' => 'wholesalerController@block'])->name('admin_wholesaler_block');

        // Business Route
        Route::get('retailers', ['uses' => 'RetailerController@index'])->name('admin_retailer');
        Route::post('retailer_grid', ['uses' => 'RetailerController@grid'])->name('admin_retailer_grid');
        Route::get('retailer/create', ['uses' => 'RetailerController@create'])->name('admin_retailer_create');
        Route::get('retailer/edit/{id}', ['uses' => 'RetailerController@edit'])->name('admin_retailer_edit');
        Route::post('retailer/store/{id?}', ['uses' => 'RetailerController@store'])->name('admin_retailer_store');
        Route::get('retailer/details/{id}', ['uses' => 'RetailerController@details'])->name('admin_retailer_details');

        // Vendor Route
        Route::get('vendors', ['uses' => 'vendorController@index'])->name('admin_vendor');
        Route::post('vendor_grid', ['uses' => 'vendorController@grid'])->name('admin_vendor_grid');
        Route::get('vendor/create', ['uses' => 'vendorController@create'])->name('admin_vendor_create');
        Route::get('vendor/edit/{id}', ['uses' => 'vendorController@edit'])->name('admin_vendor_edit');
        Route::post('vendor/store/{id?}', ['uses' => 'vendorController@store'])->name('admin_vendor_store');
        Route::get('vendor/details/{id}', ['uses' => 'vendorController@details'])->name('admin_vendor_details');
        Route::post('vendor/block', ['uses' => 'vendorController@block'])->name('admin_vendor_block');


        // Product Route
        Route::get('product', 'ProductController@index')->name('product');
        Route::post('product_grid', ['uses' => 'ProductController@grid'])->name('product_grid');
        Route::get('product/create', ['uses' => 'ProductController@create'])->name('product_create');
        Route::get('product/edit/{id}', ['uses' => 'ProductController@edit'])->name('product_edit');
        Route::get('product/details/{id}', ['uses' => 'ProductController@details'])->name('product_details');
        Route::post('product/store/{id?}', ['uses' => 'ProductController@store'])->name('product_store');
        Route::post('product/active', ['uses' => 'ProductController@change_status'])->name('product_update');
        Route::post('product/delete', ['uses' => 'ProductController@delete'])->name('product_delete');

        // Orders Route
        Route::get('enquiry', ['uses' => 'EnquiryController@index'])->name('enquiry');
        Route::post('enquiry_grid', ['uses' => 'EnquiryController@grid'])->name('enquiry_grid');

        // Contact Routes
        Route::get('contacts', ['uses' => 'ContactController@index'])->name('contacts');
        Route::post('contact_grid', ['uses' => 'ContactController@grid'])->name('contact_grid');

        // Sender ID Routes
        Route::get('senderid', ['uses' => 'SenderIdController@index'])->name('senderid');
        Route::post('sender_grid', ['uses' => 'SenderIdController@grid'])->name('sender_grid');
        Route::post('approve_sender', ['uses' => 'SenderIdController@approve'])->name('approve_sender');
        Route::post('reject_sender', ['uses' => 'SenderIdController@reject'])->name('reject_sender');
    });

});



