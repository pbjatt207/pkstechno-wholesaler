@extends('layout.admin', [
  'page_header' => 'Admin'
])

@section('content')

    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>{{ $data['title'] }}</span></h5>
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('admin_dashboard') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="#">{{ $data['title'] }}</a>
                                </li>
                                <li class="breadcrumb-item active">List
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <!-- users list start -->
                    <section class="users-list-wrapper section">

                        <div class="users-list-table">
                            <div class="card">
                                <div class="card-content">
                                    <!-- datatable start -->
                                    <div class="responsive-table">
                                        {{--                                        <table id="users-list-datatable_wrapper" class="table">--}}
                                        <table id="table" class="striped table" width="100%">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Image</th>
                                                <th>Name</th>
                                                <th>Vendor</th>
                                                <th>Price</th>
                                                <th>Amount</th>
                                                <th>Created On</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- datatable ends -->
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- users list ends -->
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div>
    <!-- END: Page Main-->
    <script>
        $(document).ready(function() {
            var datatable = $('#table').DataTable({
                "responsive": true,
                "processing" : true,
                "Filter" : true,
                "serverSide" : true,
                "sServerMethod" : "POST",
                "ajax":{
                    "url": "{{ route('product_grid') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{ csrf_token()}}"}
                },
                "iDisplayLength" : 10,
                //"aLengthMenu" : [ [ 10, 25, 50, 100, -1 ], [ 10, 25, 50, 100, "All" ] ],
                "aaSorting" : [ [ 0, 'desc' ] ],
                "aoColumnDefs": [
                    { "aTargets": [ 6 ], "orderable": false, "searchable" : false , "sClass": "text-center" }
                ],
                "columns": [
                    { "data": "id" },
                    { "data": "image" },
                    { "data": "name" },
                    { "data": "vendor" },
                    { "data": "price" },
                    { "data": "amount" },
                    { "data": "created_at" },
                ],

                // "fnServerParams": function ( aoData ) {
                //     aoData.push(
                //         { "name": "csrf_test_name", "value": $.cookie("csrf_cookie_name") }
                //     );
                // },
                "dom": 'Tfg<"search_box pull-right dt_search_box"><"pull-left text-right report_table_length"l>t<"pull-left"i><"pull-right"p>'
            });

            $(document).on("click",".updateStatus", function(){
                var id = $(this).data('id');
                swal({
                    title: "Are you sure?",
                    text: "Do you want to Update this product!",
                    icon: 'warning',
                    dangerMode: true,
                    buttons: {
                        cancel: 'No',
                        delete: 'Yes'
                    }
                }).then(function (willDelete) {
                    if (willDelete) {
                        // swal("Poof! Your imaginary file has been deleted!", {
                        //     icon: "success",
                        // });
                        $.ajax({
                            type:'POST',
                            url:"{{ route('vendor_product_update') }}",
                            data:{id :id, _token: "{{ csrf_token()}}" },
                            success: function(res) {
                                console.log(res.msg);
                                datatable.ajax.reload();
                                M.toast({html: res.msg, classes: 'rounded'});
                            }
                        });
                    }
                });

            });

            $(document).on("click",".delete", function(){
                var id = $(this).data('id');
                swal({
                    title: "Are you sure?",
                    text: "Do you want to Delete this product!",
                    icon: 'warning',
                    dangerMode: true,
                    buttons: {
                        cancel: 'No',
                        delete: 'Yes'
                    }
                }).then(function (willDelete) {
                    if (willDelete) {
                        // swal("Poof! Your imaginary file has been deleted!", {
                        //     icon: "success",
                        // });
                        $.ajax({
                            type:'POST',
                            url:"{{ route('vendor_product_delete') }}",
                            data:{id :id, _token: "{{ csrf_token()}}" },
                            success: function(res) {
                                datatable.ajax.reload();
                                M.toast({html: res.msg, classes: 'rounded'});
                            }
                        });
                    }
                });

            });
        } );
    </script>
@endsection
