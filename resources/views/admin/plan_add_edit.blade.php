@extends('layout.admin', [
  'page_header' => 'Admin'
])

@section('content')

    <link rel="stylesheet" type="text/css" href="{{ url('assets/vendors/dropify/css/dropify.min.css') }}">

    <script src="{{ url('assets/vendors/dropify/js/dropify.min.js') }}"></script>
    <script src="{{ url('assets/js/scripts/form-file-uploads.min.js') }}"></script>

    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>{{ $data['title'] }}</span></h5>
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{ route('plans') }}">{{ $data['title'] }}</a>
                                </li>
                                <li class="breadcrumb-item active">Add
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <div class="section">

                        <!-- HTML VALIDATION  -->

                        <div class="row">
                            <div class="col s12">
                                <div id="html-validations" class="card card-tabs">
                                    <div class="card-content">
                                        <div class="card-title">
                                            <div class="row">
                                                <div class="col s12 m6 l10">
                                                    <h4 class="card-title">Add Plan</h4>
                                                </div>
                                                <div class="col s12 m6 l2">
                                                </div>
                                            </div>
                                        </div>
                                        <div id="html-view-validations">
                                            <form enctype="multipart/form-data" class="formValidate0" id="formValidate0" method="post" action="{{ route('store_plan', $id) }}">
                                                @csrf
                                                <div class="row">
                                                    <div class="input-field col s12">
                                                        @php $val = !empty($plan->title) ?  $plan->title : ''; @endphp
                                                        <label for="uname0">Title*</label>
                                                        <input class="validate" value="{{ old('title', $val) }}" required id="uname0" name="title" type="text">
                                                        <small class="text-danger">{{ $errors->first('title') }}</small>
                                                    </div>
                                                    <div class="input-field col s6">
                                                        @php $val = !empty($plan->message) ?  $plan->message : ''; @endphp
                                                        <label for="mobile0">No of Messages *</label>
                                                        <input class="validate" value="{{ old('message', $val) }}" required id="mobile0" type="number" name="message">
                                                        <small class="text-danger">{{ $errors->first('message') }}</small>
                                                    </div>
                                                    <div class="input-field col s6">
                                                        @php $val = !empty($plan->price) ?  $plan->price : 0; @endphp
                                                        <label for="price">Price *</label>
                                                        <input class="validate" value="{{ old('price', $val) }}" required id="price" type="number" name="price">
                                                        <small class="text-danger">{{ $errors->first('price') }}</small>
                                                    </div>
                                                    <div class="input-field col s6">
{{--                                                        <label for="">Discount Type *</label>--}}
                                                        <div class="">
                                                            @php $val = !empty($plan->discount_type) ?  $plan->discount_type : 'A'; @endphp
                                                            <select class="select2 browser-default" id="discount_type" name="discount_type">
                                                                <option value="A" @if($val == 'A') selected @endif >Amount</option>
                                                                <option value="P" @if($val == 'P') selected @endif >Percentage</option>
                                                            </select>
                                                            <small class="text-danger">{{ $errors->first('discount_type') }}</small>
                                                        </div>
                                                    </div>
                                                    <div class="input-field col s6">
                                                        @php $val = !empty($plan->discount) ?  $plan->discount : 0; @endphp
                                                        <label for="discount">Discount Value *</label>
{{--                                                        <input class="validate" value="{{ old('discount', $val) }}" required id="discount" type="number" name="discount">--}}
                                                        <input type="number" id="discount" name="discount" value="{{ old('discount', $val) }}" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==4) return false;" />
                                                        <small class="text-danger">{{ $errors->first('discount') }}</small>
                                                    </div>
                                                    <div class="input-field col s6">
                                                        @php $val = !empty($plan->amount) ?  $plan->amount : 0; @endphp
                                                        <label for="amount">Amount *</label>
                                                        <input class="validate" readonly value="{{ old('amount', $val) }}" required id="amount" type="number" name="amount">
                                                        <small class="text-danger">{{ $errors->first('amount') }}</small>
                                                    </div>
                                                    <div class="input-field col s6">
                                                        @php $val = !empty($plan->commission_rate) ?  $plan->commission_rate : 0; @endphp
                                                        <label for="commission_rate">Commission Rate *</label>
                                                        <input class="" value="{{ old('commission_rate', $val) }}" required id="commission_rate" max="100" type="number" name="commission_rate">
                                                        <small class="text-danger">{{ $errors->first('commission_rate') }}</small>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col s12">
                                                        <div class="input-field">
                                                            @php $val = !empty($plan->description) ?  $plan->description : ''; @endphp
                                                            <label for="descs">Description *</label>
                                                            <textarea class="materialize-textarea" name="description" id="descs" rows="10">{{ old('description', $val) }}</textarea>
{{--                                                            <input class="validate" value="{{ old('message', $val) }}" required id="mobile0" type="number" name="message">--}}
                                                            <small class="text-danger">{{ $errors->first('description') }}</small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="divider mb-1 mt-1"></div>
                                                <div class="row">
                                                    <div class="input-field col s12">
                                                        <button class="btn waves-effect waves-light right" type="submit">Submit
                                                            <i class="material-icons right">send</i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div>
    <!-- END: Page Main-->

    <script>
        $(document).ready(function(){
            $("#sender_id").on("keyup",function (){
                var sender_id = $(this).val();
                $(this).val(sender_id.toUpperCase());
                if(sender_id.length == 6){
                    $.ajax({
                        type:'POST',
                        url:"{{ route('check_sender') }}",
                        data:{sender_id :sender_id, _token: "{{ csrf_token()}}" },
                        success:function(data) {

                            if(data.status){
                                $(".sender_message").html(data.message).css('color', '#7223a2');
                            } else {
                                $(".sender_message").html(data.message).css('color', '#ff4081');
                            }
                        }
                    });
                } else {
                    $(".sender_message").html('Please Enter Unique 6 Digit Sender ID').css('color', '#6b6f82');
                }
            });

            discount_value();
            $("#price").keyup(function () {
                var price = parseInt($(this).val());
                $(this).val(price);
                discount_value();
            });

            $("#discount").keyup(function () {
                var type = $("#discount_type").val();
                var typeval = $("#discount").val();

                if(type == 'A' ){
                    $("#discount").val(parseInt(typeval));
                }

                discount_value();
            });
            $("#discount_type").on('change', function () {
                var type = $(this).val();
                var typeval = $("#discount").val(0);

                discount_value();
            });
        });

        function discount_value() {
            var type = $("#discount_type");
            var typeval = $("#discount");
            var price = $("#price");
            var amount = $("#amount");

            var Price = price.val();
            var discount = parseFloat(typeval.val());

            if(Price != ''){
                if(discount != '' ){
                    if(type.val() == 'P' && discount < 100){
                        var disval = parseFloat(Price * discount/100);
                        console.log(disval);
                        Price = Price - disval;
                    } else if(type.val() == 'A') {
                        Price = Price - discount;
                    }
                }
            }
            amount.val(Price);
        }
    </script>
@endsection
