@extends('layout.admin', [
  'page_header' => 'Admin'
])

@section('content')
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/chartist-js/chartist.min.css') }}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/chartist-js/chartist-plugin-tooltip.css') }}">--}}


    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="col s12">
                <div class="container">
                    <div class="section">
                        <!-- Current balance & total transactions cards-->
                        <div class="row vertical-modern-dashboard">
                            <div class="col s12 m4 l4">
                                <!-- Total Income -->
                                <div class="card padding-4 animate fadeLeft">
                                    <div class="row">
                                        <div class="col s5 m5">
                                            <h5 class="mb-0">{{ $last24hr_product }}</h5>
                                            <p class="no-margin">New</p>
                                            <p class="mb-0 pt-8">{{ $total_product }}</p>
                                        </div>
                                        <div class="col s7 m7 right-align">
                                            <i
                                                class="material-icons background-round mt-5 mb-5 gradient-45deg-purple-amber gradient-shadow white-text">shopping_cart</i>
                                            <p class="mb-0">Total Products</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m6 l4">
                                <div class="card padding-4 animate fadeLeft">
                                    <div class="row">
                                        <div class="col s5 m5">
                                            <h5 class="mb-0">{{ $last24hr_wholesaler }}</h5>
                                            <p class="no-margin">New</p>
                                            <p class="mb-0 pt-8">{{ $total_wholesaler }}</p>
                                        </div>
                                        <div class="col s7 m7 right-align">
                                            <i
                                                class="material-icons background-round mt-5 mb-5 gradient-45deg-purple-amber gradient-shadow white-text">perm_identity</i>
                                            <p class="mb-0">Total Wholesaler</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m6 l4">
                                <div class="card padding-4 animate fadeLeft">
                                    <div class="row">
                                        <div class="col s5 m5">
                                            <h5 class="mb-0">{{ $last24hr_vendor }}</h5>
                                            <p class="no-margin">New</p>
                                            <p class="mb-0 pt-8">{{ $total_vendor }}</p>
                                        </div>
                                        <div class="col s7 m7 right-align">
                                            <i
                                                class="material-icons background-round mt-5 mb-5 gradient-45deg-purple-amber gradient-shadow white-text">perm_identity</i>
                                            <p class="mb-0">Total Vendor</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div>
    <!-- END: Page Main-->


{{--    <script src="{{ asset('assets/vendors/chartjs/chart.min.js') }}"></script>--}}
{{--    <script src="{{ asset('assets/vendors/chartist-js/chartist.min.js') }}"></script>--}}
{{--    <script src="{{ asset('assets/vendors/chartist-js/chartist-plugin-tooltip.js') }}"></script>--}}
{{--    <script src="{{ asset('assets/vendors/chartist-js/chartist-plugin-fill-donut.min.js') }}"></script>--}}
{{--    <script src="{{asset('assets/js/scripts/dashboard-modern.js')}}" type="text/javascript"></script>--}}



@endsection
