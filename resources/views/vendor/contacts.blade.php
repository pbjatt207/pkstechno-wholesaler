@extends('layout.admin', [
  'page_header' => 'Admin'
])

@section('content')

    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>{{ $data['title'] }}</span></h5>
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('admin/dashboard') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="#">{{ $data['title'] }}</a>
                                </li>
                                <li class="breadcrumb-item active">Contact List
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <!-- users list start -->
                    <section class="users-list-wrapper section">

                        <div class="users-list-table">
                            <div class="card">
                                <div class="card-content">
                                    <!-- datatable start -->
                                    <div class="responsive-table">
{{--                                        <table id="users-list-datatable_wrapper" class="table">--}}
                                        <table id="table" class="table">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Name</th>
                                                    <th>Phone</th>
                                                    <th>Date & Time</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- datatable ends -->
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- users list ends -->
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div>
    <!-- END: Page Main-->
    <script>
        $(document).ready(function() {
            var gridUrl = '{{ route('contact_grid') }}';
            $('#table').DataTable({
                "responsive": true,
                "processing" : true,
                "Filter" : true,
                "serverSide" : true,
                "sServerMethod" : "POST",
                "ajax":{
                    "url": gridUrl,
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{ csrf_token()}}"}
                },
                "iDisplayLength" : 100,
                "aLengthMenu" : [ [ 100, 250, 500, 1000, -1 ], [ 100, 250, 500, 1000, "All" ] ],
                "aaSorting" : [ [ 0, 'desc' ] ],
                "columns": [
                    { "data": "id" },
                    { "data": "name" },
                    { "data": "phone" },
                    { "data": "created_at" },
                ],

                // "fnServerParams": function ( aoData ) {
                //     aoData.push(
                //         { "name": "csrf_test_name", "value": $.cookie("csrf_cookie_name") }
                //     );
                // },
                "dom": 'Tfg<"search_box pull-right dt_search_box"><"pull-left text-right report_table_length"l>t<"pull-left"i><"pull-right"p>'
            });
        } );
    </script>
@endsection
