@extends('layout.admin', [
  'page_header' => 'Admin'
])

@section('content')

    <link rel="stylesheet" type="text/css" href="{{ url('assets/vendors/dropify/css/dropify.min.css') }}">

    <script src="{{ url('assets/vendors/dropify/js/dropify.min.js') }}"></script>
    <script src="{{ url('assets/js/scripts/form-file-uploads.min.js') }}"></script>

    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0">
                                <span>{{ $data['title'] }}</span>
                            </h5>
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('admin/dashboard') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{ route('admin_retailer') }}">{{ $data['title'] }}</a>
                                </li>
                                <li class="breadcrumb-item active">View Details
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <!-- users view start -->
                    <div class="section users-view">
                        <!-- users view media object start -->
                        <div class="card-panel">
                            <div class="row">
                                <div class="col s12 m7">
                                    <div class="display-flex media">
                                        <a href="#" class="avatar">
                                            @php $image = asset('assets/images/avatar/avatar-15.png');
                                                if(!empty($user->image)){
                                                    $image = url('images/users/'.$user->image);
                                                }
                                            @endphp
                                            <img src="{{ $image }}" alt="users view avatar" class="z-depth-4 circle"
                                                 height="64" width="64">
                                        </a>
                                        <div class="media-body">
                                            <h6 class="media-heading">
                                                <span class="users-view-name">{{ strtoupper($user->name) }}</span>
                                            </h6>
{{--                                            <span>Sender ID:</span>--}}
{{--                                            <span class="users-view-id">{{ $user->sender_id }}</span>--}}
                                            @if($referraluser)

                                            <span>Invited By:</span>
                                            <span class="users-view-id">{{ $referraluser->name }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12 m5 quick-action-btns display-flex justify-content-end align-items-center pt-2">
{{--                                    <a href="app-email.html" class="btn-small btn-light-indigo"><i class="material-icons">mail_outline</i></a>--}}
{{--                                    <a href="user-profile-page.html" class="btn-small btn-light-indigo">Profile</a>--}}
                                    <a href="{{ route('admin_retailer_edit', $user->id) }}" class="btn-small indigo">Edit</a>
                                </div>
                            </div>
                        </div>

                        @if($user->sum)
                        <div class="card">
                            <div class="card-content">
                                <div class="row indigo lighten-5 border-radius-4    ">
                                    <div class="col s12 m4 users-view-timeline">
                                        <h6 class="indigo-text m-0">Available: <span>{{ $user->balance }}</span></h6>
                                    </div>
                                    <div class="col s12 m4 users-view-timeline">
                                        <h6 class="indigo-text m-0">Used: <span>{{ $user->sum - $user->balance }}</span></h6>
                                    </div>
                                    <div class="col s12 m4 users-view-timeline">
                                        <h6 class="indigo-text m-0">Purchased: <span>{{ $user->sum }}</span></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="card">
                            <div class="card-content">
                                <div class="row">
                                    <div class="col s12 m6">
                                        <table class="striped">
                                            <tbody>
                                            <tr>
                                                <td>Mobile:</td>
                                                <td>{{ $user->mobile }}</td>
                                            </tr>
                                            <tr>
                                                <td>Email:</td>
                                                <td>{{ $user->email }}</td>
                                            </tr>
                                            <tr>
                                                <td>Is Active:</td>
                                                <td class="users-view-verified">{{ $user->is_active ? 'Yes' : 'No' }}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col s12 m6">
                                        <table class="striped">
                                            <tbody>
                                            <tr>
                                                <td>Registered:</td>
                                                <td>{{ date('d/m/Y h:i A',strtotime($user->created_at)) }}</td>
                                            </tr>
                                            <tr>
                                                <td>Latest Activity:</td>
                                                <td>{{ date('d/m/Y',strtotime($user->created_at)) }}</td>
                                            </tr>
                                            <tr>
                                                <td>Status:</td>
                                                <td><span class=" users-view-status chip green lighten-5 green-text">Active</span></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- users view card data ends -->

                        <!-- users view card details start -->
                        @if($user->senderIds)
                        <div class="card">
                            <div class="card-content">
                                <div class="row content_section indigo lighten-5 border-radius-4">
                                    <div class="col s12 m12 users-view-timeline">
                                        <h5 class="indigo-text m-0">Sender Id history:</h5>
                                    </div>
                                </div>

                                <div class="row mt-4 content_data">
                                    <div class="col s12">
                                        <table class="table striped bordered" id="business_groups">
                                            <thead>
                                                <tr>
{{--                                                    <th width="100px">SN</th>--}}
                                                    <th>Sender ID</th>
                                                    <th>Created On</th>
                                                    <th>Status</th>
                                                    <th>Approved On</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($user->senderIds as $key => $each)
                                                    @php
                                                        $is_approved = "Pending";
                                                        $approve_date = '';
                                                        if($each->is_approved){
                                                            $is_approved = "Approved";
                                                        } else if(!$each->is_approved && $each->approved_on > 0){
                                                            $is_approved = "Rejected";
                                                        }

                                                        if($each->approved_on > 0){
                                                            $approve_date = date('d/m/Y h:i A', strtotime($each->approved_on));
                                                        }
                                                    @endphp
                                                <tr>
{{--                                                    <td>{{ $key+1 }}</td>--}}
                                                    <td>{{ $each->sender_id }}</td>
                                                    <td>{{ date('d/m/Y h:i A', strtotime($each->created_at)) }}</td>
                                                    <td>{{ $is_approved }}</td>
                                                    <td>{{ $approve_date }}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @if($user->groups)
                        <div class="card">
                            <div class="card-content">
                                <div class="row content_section indigo lighten-5 border-radius-4">
                                    <div class="col s12 m12 users-view-timeline">
                                        <h5 class="indigo-text m-0">Groups:</h5>
                                    </div>
                                </div>

                                <div class="row mt-4 content_data">
                                    <div class="col s12">
                                        <table class="table striped bordered" id="business_groups">
                                            <thead>
                                                <tr>
{{--                                                    <th width="100px">SN</th>--}}
                                                    <th>Group Name</th>
                                                    <th>No of Contacts</th>
                                                    <th>Messages sent</th>
                                                    <th>Created On</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($user->groups as $key => $each)
                                                <tr>
{{--                                                    <td>{{ $key+1 }}</td>--}}
                                                    <td>{{ $each->name }}</td>
                                                    <td>{{ $each->group_contacts_count }}</td>
                                                    <td>{{ $each->messages_count }}</td>
                                                    <td>{{ date('d/m/Y h:i A', strtotime($each->created_at)) }}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
{{--                        <div class="card">--}}
{{--                            <div class="card-content">--}}
{{--                                <div class="row content_section indigo lighten-5 border-radius-4">--}}
{{--                                    <div class="col s12 m12 users-view-timeline">--}}
{{--                                        <h5 class="indigo-text m-0">Purchased Plans:</h5>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                @if($plan_purchased)--}}
{{--                                <div class="row mt-4 content_data" style="display: none">--}}
{{--                                    <div class="col s12">--}}
{{--                                        <table class="table striped bordered" id="plans_purchased">--}}
{{--                                            <thead>--}}
{{--                                                <tr>--}}
{{--                                                    <th width="100px">SN</th>--}}
{{--                                                    <th>Title</th>--}}
{{--                                                    <th>Amount</th>--}}
{{--                                                    <th>Message</th>--}}
{{--                                                    <th>Purchased On</th>--}}
{{--                                                </tr>--}}
{{--                                            </thead>--}}
{{--                                            <tbody>--}}
{{--                                                @foreach($plan_purchased as $key => $each)--}}
{{--                                                <tr>--}}
{{--                                                    <td>{{ $key+1 }}</td>--}}
{{--                                                    <td>{{ $each->title }}</td>--}}
{{--                                                    <td>&#8377;{{ $each->amount }}</td>--}}
{{--                                                    <td>{{ $each->message }}</td>--}}
{{--                                                    <td>{{ date('d/m/Y h:i A', strtotime($each->created_at)) }}</td>--}}
{{--                                                </tr>--}}
{{--                                                @endforeach--}}
{{--                                            </tbody>--}}
{{--                                        </table>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="card">--}}
{{--                            <div class="card-content">--}}
{{--                                <div class="row content_section indigo lighten-5 border-radius-4 ">--}}
{{--                                    <div class="col s12 m12 users-view-timeline">--}}
{{--                                        <h5 class="indigo-text m-0">Documents:</h5>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="row mt-4 content_data" style="display: none" >--}}
{{--                                    @if(!empty($user->document->pan_card))--}}
{{--                                    <div class="col s4">--}}
{{--                                        <label>Pan Card Image</label>--}}
{{--                                        @php $image = url('images/documents/'.$user->document->pan_card) @endphp--}}
{{--                                        <a href="{{ $image }}" target="_blank">--}}
{{--                                            <div class="dropify-wrapper img_preview_box">--}}
{{--                                                <img src="{{ $image }}">--}}
{{--                                            </div>--}}
{{--                                        </a>--}}
{{--                                    </div>--}}
{{--                                    @endif--}}
{{--                                    @if(!empty($user->document->aadhar_front))--}}
{{--                                    <div class="col s4">--}}
{{--                                        <label>Aadhar Front Image</label>--}}
{{--                                        @php $image = url('images/documents/'.$user->document->aadhar_front) @endphp--}}
{{--                                        <a href="{{ $image }}" target="_blank">--}}
{{--                                            <div class="dropify-wrapper img_preview_box">--}}
{{--                                                <img src="{{ $image }}">--}}
{{--                                            </div>--}}
{{--                                        </a>--}}
{{--                                    </div>--}}
{{--                                    @endif--}}
{{--                                    @if(!empty($user->document->aadhar_back))--}}
{{--                                    <div class="col s4">--}}
{{--                                        <label>Aadhar Back Image</label>--}}
{{--                                        @php $image = url('images/documents/'.$user->document->aadhar_back) @endphp--}}
{{--                                        <a href="{{ $image }}" target="_blank">--}}
{{--                                            <div class="dropify-wrapper img_preview_box">--}}
{{--                                                <img src="{{ $image }}">--}}
{{--                                            </div>--}}
{{--                                        </a>--}}
{{--                                    </div>--}}
{{--                                    @endif--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <!-- users view card details ends -->

                    </div>
                    <!-- users view ends -->

                    <!-- END RIGHT SIDEBAR NAV -->
                    <div style="bottom: 50px; right: 19px;" class="fixed-action-btn direction-top">
                        <a class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow" href="{{ route('admin_retailer_create') }}"><i class="material-icons">add</i></a>
                    </div>
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div>
    <!-- END: Page Main-->
    <script>
        $(document).ready(function(){
            $(".content_section").on("click", function(){
                $(this).closest('.card-content').find('.content_data').slideToggle();
            });
            $("#plans_purchased").DataTable({
                "responsive": true,
                "Filter" : true,
                "iDisplayLength" : 10,
                "aLengthMenu" : [ [ 10, 25, 50, 100, -1 ], [ 10, 25, 50, 100, "All" ] ],
                "aaSorting" : [ [ 3, 'desc' ] ],
            });
            $("#business_groups").DataTable({
                "responsive": true,
                "Filter" : true,
                "iDisplayLength" : 10,
                "aLengthMenu" : [ [ 10, 25, 50, 100, -1 ], [ 10, 25, 50, 100, "All" ] ],
                "aaSorting" : [ [ 1, 'desc' ] ],
            });

        });
    </script>
@endsection
