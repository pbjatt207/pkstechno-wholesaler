@extends('layout.vendor', [
  'page_header' => 'Vendor'
])

@section('content')

    <link rel="stylesheet" type="text/css" href="{{ url('assets/vendors/dropify/css/dropify.min.css') }}">

    <script src="{{ url('assets/vendors/dropify/js/dropify.min.js') }}"></script>
    <script src="{{ url('assets/js/scripts/form-file-uploads.min.js') }}"></script>

    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>{{ $data['title'] }}</span></h5>
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('vendor_dashboard') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{ route('vendor_product') }}">{{ $data['title'] }}</a>
                                </li>
                                <li class="breadcrumb-item active">Add
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <div class="section">

                        <!-- HTML VALIDATION  -->

                        <div class="row">
                            <div class="col s12">
                                <div id="html-validations" class="card card-tabs">
                                    <div class="card-content">
                                        <div class="card-title">
                                            <div class="row">
                                                <div class="col s12 m6 l10">
                                                    <h4 class="card-title"> Product Information</h4>
                                                </div>
                                                <div class="col s12 m6 l2">
                                                </div>
                                            </div>
                                        </div>
                                        <div id="html-view-validations">
                                            <form enctype="multipart/form-data" class="formValidate0" id="formValidate0" method="post" action="{{ route('vendor_product_store',$id) }}">
                                                @csrf
{{--                                                <div class="row">--}}
{{--                                                    <div class="input-field col s6">--}}
{{--                                                        <label for="sender_id">Sender ID *</label>--}}
{{--                                                        @php $val = !empty($info->sender_id) ?  $info->sender_id : ''; @endphp--}}
{{--                                                        <input class="" required id="sender_id" type="text" maxlength="6" name="sender_id" value="{{ old('sender_id', $val) }}">--}}
{{--                                                        <span class="helper-text sender_message">Please Enter 6 Digit Unique Sender ID</span>--}}
{{--                                                        <small class="text-danger">{{ $errors->first('sender_id') }}</small>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="col s6">--}}
{{--                                                        <div class="input-field">--}}
{{--                                                            @php $val = !empty($info->referred_by) ?  $info->referred_by : ''; @endphp--}}
{{--                                                            <select class="select2 browser-default" name="referred_by" id="default-select" data-select2-id="default-select" tabindex="-1" aria-hidden="true">--}}
{{--                                                                <option value="0" >Admin</option>--}}
{{--                                                                @foreach($dealers as $key => $each)--}}
{{--                                                                    <option value="{{ $each->id }}" @if(old('referred_by', $val) == $each->id)) selected @endif >{{ $each->referral_code.' | '.$each->name }}</option>--}}
{{--                                                                @endforeach--}}
{{--                                                            </select>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
                                                <div class="row">
                                                    <div class="input-field col s12">
                                                        @php $val = !empty($info->name) ?  $info->name : ''; @endphp
                                                        <label for="uname0">Product Name*</label>
                                                        <input class="validate" value="{{ old('name', $val) }}" required id="uname0" name="name" type="text">
                                                        <small class="text-danger">{{ $errors->first('name') }}</small>
                                                    </div>
                                                    <div class="input-field col s4">
                                                        @php $val = !empty($info->price) ?  $info->price : 0; @endphp
                                                        <label for="price">Price*</label>
                                                        <input class="validate" value="{{ old('price', $val) }}" required id="price" name="price" type="number">
                                                        <small class="text-danger">{{ $errors->first('price') }}</small>
                                                    </div>
                                                    <div class="col s4">
                                                        <div class="input-field">
                                                            <label for="">Discount Type*</label>
                                                            @php $val = !empty($info->discount) ?  $info->discount : 'amount'; @endphp
                                                            <select class="select2 browser-default" name="discount">
                                                                <option value="amount" @if($val == 'amount') selected @endif >Amount</option>
                                                                <option value="percent" @if($val == 'percent') selected @endif >Percent</option>
                                                            </select>
                                                            <small class="text-danger">{{ $errors->first('price') }}</small>
                                                        </div>
                                                    </div>
                                                    <div class="input-field col s4">
                                                        @php $val = !empty($info->discount_value) ?  $info->discount_value : 0; @endphp
                                                        <label for="discount_value">Discount Value*</label>
                                                        <input class="validate" value="{{ old('discount_value', $val) }}" id="discount_value" name="discount_value" type="number">
                                                        <small class="text-danger">{{ $errors->first('discount_value') }}</small>
                                                    </div>
                                                    <div class="input-field col s6">
                                                        @php $val = !empty($info->amount) ?  $info->amount : ''; @endphp
                                                        <label for="amount">Amount*</label>
                                                        <input class="validate" value="{{ old('amount', $val) }}" readonly required id="amount" name="amount" type="number">
                                                        <small class="text-danger">{{ $errors->first('amount') }}</small>
                                                    </div>

                                                    <div class="input-field col s12">@php $val = !empty($info->description) ?  $info->description : ''; @endphp
                                                        <label for="address">Description *</label>
                                                        <textarea class="materialize-textarea validate" rows="7" id="address" name="description">{{ old('address', $val) }}</textarea>
                                                        <small class="text-danger">{{ $errors->first('address') }}</small>
                                                    </div>
                                                </div>
                                                <div class="divider mb-1 mt-1"></div>
                                                <div class="row">
                                                    <div class="col s4 m4 l4">
                                                        @php
                                                                $image = '';
                                                                if(!empty($info->image)){
                                                                    $image = url('/images/product/'.$info->image);
                                                                }
                                                        @endphp
                                                        <p>Product Image (Max 2MB).</p>
                                                        <input type="file" name="image" accept="image/*" data-default-file="{{ $image }}" id="input-file-events" class="dropify-event" data-max-file-size="2M" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="input-field col s12">
                                                        <button class="btn waves-effect waves-light right" type="submit">Submit
                                                            <i class="material-icons right">send</i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div>
    <!-- END: Page Main-->

    <script>
        $(document).ready(function(){
            $("#price").on("keyup keypress", function () {
                var price = $(this).val();
                $(this).val(parseInt(price));
                var amount_input = $("#amount");

                amount_input.val(price);


            });
            $("#sender_id").on("keyup",function (){
                var sender_id = $(this).val();
                $(this).val(sender_id.toUpperCase());
                if(sender_id.length == 6){
                    $.ajax({
                        type:'POST',
                        url:"{{ route('check_sender') }}",
                        data:{sender_id :sender_id, _token: "{{ csrf_token()}}" },
                        success:function(data) {

                            if(data.status){
                                $(".sender_message").html(data.message).css('color', '#7223a2');
                            } else {
                                $(".sender_message").html(data.message).css('color', '#ff4081');
                            }
                        }
                    });
                } else {
                    $(".sender_message").html('Please Enter Unique 6 Digit Sender ID').css('color', '#6b6f82');
                }
            });
        });
    </script>
@endsection
