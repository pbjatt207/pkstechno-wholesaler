@extends('layout.vendor', [
  'page_header' => 'Vendor'
])

@section('content')

    <link rel="stylesheet" type="text/css" href="{{ url('assets/vendors/dropify/css/dropify.min.css') }}">

    <script src="{{ url('assets/vendors/dropify/js/dropify.min.js') }}"></script>
    <script src="{{ url('assets/js/scripts/form-file-uploads.min.js') }}"></script>

    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0">
                                <span>{{ $data['title'] }}</span>
                            </h5>
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('vendor_dashboard') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{ route('vendor_wholesaler') }}">{{ $data['title'] }}</a>
                                </li>
                                <li class="breadcrumb-item active">View Details
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <!-- users view start -->
                    <div class="section users-view">
                        <!-- users view media object start -->
                        <div class="card-panel">
                            <div class="row">
                                <div class="col s12 m7">
                                    <div class="display-flex media">
                                        <a href="#" class="avatar">
                                            <img src="{{ asset('assets/images/avatar/avatar-15.png') }}" alt="users view avatar" class="z-depth-4 circle"
                                                 height="64" width="64">
                                        </a>
                                        <div class="media-body">
                                            <h6 class="media-heading">
                                                <span class="users-view-name">{{ $user->name }}</span>
                                            </h6>
                                            <span>Code:</span>
                                            <span class="users-view-id">{{ $user->referral_code }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12 m5 quick-action-btns display-flex justify-content-end align-items-center pt-2">
{{--                                    <a href="app-email.html" class="btn-small btn-light-indigo"><i class="material-icons">mail_outline</i></a>--}}
{{--                                    <a href="user-profile-page.html" class="btn-small btn-light-indigo">Profile</a>--}}
                                    <a href="{{ route('vendor_wholesaler_edit', $user->id) }}" class="btn-small indigo">Edit</a>
                                </div>
                            </div>
                        </div>
                        <!-- users view media object ends -->
                        <!-- users view card data start -->
                        <div class="card">
                            <div class="card-content">
                                <div class="row">
                                    <div class="col s12 m6">
                                        <table class="striped">
                                            <tbody>
                                            <tr>
                                                <td>Shop:</td>
                                                <td>{{ $user->shop }}</td>
                                            </tr>
                                            <tr>
                                                <td>Email:</td>
                                                <td>{{ $user->email }}</td>
                                            </tr>
                                            <tr>
                                                <td>Mobile Number:</td>
                                                <td class="users-view-verified">{{ $user->mobile }}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col s12 m6">
                                        <table class="striped">
                                            <tbody>
                                            <tr>
                                                <td>Address:</td>
                                                <td>{{ $user->address }}</td>
                                            </tr>
                                            <tr>
                                                <td>Registered:</td>
                                                <td>{{ date('d/m/Y h:i A',strtotime($user->created_at)) }}</td>
                                            </tr>
                                            <tr>
                                                <td>Verified:</td>
                                                <td class="users-view-verified">{{ $user->is_verified == 'Y' ? 'Yes': 'No' }}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- users view card data ends -->

                        @if(COUNT($referralBusiness) > 0)
                            <div class="card">
                                <div class="card-content">
                                    <div class="row content_section indigo lighten-5 border-radius-4">
                                        <div class="col s12 m12 users-view-timeline">
                                            <h5 class="indigo-text m-0">Invited Business:</h5>
                                        </div>
                                    </div>

                                    <div class="row mt-4 content_data">
                                        <div class="col s12">
                                            <table class="table striped bordered" id="invite_business">
                                                <thead>
                                                    <tr>
                                                        <th>Business</th>
                                                        <th>Mobile Number</th>
                                                        <th>Balance SMS</th>
                                                        <th>Invited On</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($referralBusiness as $key => $each)
                                                    <tr>
                                                        <td>{{ $each->name }}</td>
                                                        <td>{{ $each->mobile }}</td>
                                                        <td>{{ $each->sum - $each->used }}</td>
                                                        <td>{{ date('d/m/Y h:i A', strtotime($each->created_at)) }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif


                        <!-- users view card details start -->
                        <div class="card">
                            <div class="card-content">
                                <div class="row indigo lighten-5 border-radius-4 mb-2">
                                    <div class="col s12 m12 users-view-timeline">
                                        <h5 class="indigo-text m-0">Documents:</h5>
                                    </div>
                                </div>

                                <div class="row">
                                    @if(!empty($user->document->pan_card))
                                        <div class="col s4">
                                            <label>Pan Card Image</label>
                                            @php $image = url('images/documents/'.$user->document->pan_card) @endphp
                                            <a href="{{ $image }}" target="_blank">
                                                <div class="dropify-wrapper img_preview_box">
                                                    <img src="{{ $image }}">
                                                </div>
                                            </a>
                                        </div>
                                    @endif
                                    @if(!empty($user->document->aadhar_front))
                                        <div class="col s4">
                                            <label>Aadhar Front Image</label>
                                            @php $image = url('images/documents/'.$user->document->aadhar_front) @endphp
                                            <a href="{{ $image }}" target="_blank">
                                                <div class="dropify-wrapper img_preview_box">
                                                    <img src="{{ $image }}">
                                                </div>
                                            </a>
                                        </div>
                                    @endif
                                    @if(!empty($user->document->aadhar_back))
                                        <div class="col s4">
                                            <label>Aadhar Back Image</label>
                                            @php $image = url('images/documents/'.$user->document->aadhar_back) @endphp
                                            <a href="{{ $image }}" target="_blank">
                                                <div class="dropify-wrapper img_preview_box">
                                                    <img src="{{ $image }}">
                                                </div>
                                            </a>
                                        </div>
                                    @endif
                                </div>
                                <!-- </div> -->
                            </div>
                        </div>
                        <!-- users view card details ends -->

                    </div>
                    <!-- users view ends -->
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div>
    <!-- END: Page Main-->

    <script>
        $(document).ready(function(){
            $(".content_section").on("click", function(){
                $(this).closest('.card-content').find('.content_data').slideToggle();
            });
            $("#invite_business").DataTable({
                "responsive": true,
                "Filter" : true,
                "iDisplayLength" : 10,
                "aLengthMenu" : [ [ 10, 25, 50, 100, -1 ], [ 10, 25, 50, 100, "All" ] ],
                "aaSorting" : [ [ 3, 'desc' ] ],
            });
            $("#dealer_commission").DataTable({
                "responsive": true,
                "Filter" : true,
                "iDisplayLength" : 10,
                "aLengthMenu" : [ [ 10, 25, 50, 100, -1 ], [ 10, 25, 50, 100, "All" ] ],
                "aaSorting" : [ [4 , 'desc' ] ],
            });

        });
    </script>
@endsection
