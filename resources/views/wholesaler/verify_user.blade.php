@extends('layout.dealersingle', [
  'page_header' => 'Dealer'
])

@section('content')

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/lock.min.css') }}">
    <script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
    <div id="lock-screen" class="row">
        <div class="col s12 m6 l4 z-depth-4 card-panel border-radius-6 forgot-card bg-opacity-8">
            <form class="login-form" method="post" >
                @csrf
                @if(Session::has('success'))
                    <div class="card-alert card gradient-45deg-green-teal" >
                        <div class="card-content white-text">
                            {{ Session::get('success') }}
                        </div>
                    </div>
                @endif
                @if(Session::has('error'))
                    <div class="card-alert card gradient-45deg-red-pink" >
                        <div class="card-content white-text">
                            {{ Session::get('error') }}
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="input-field col s12 center-align mt-10">
                        <img class="z-depth-4 circle responsive-img" width="100" src="{{ asset('assets/images/user/4.jpg') }}" alt="">
                        <h5>{{ $user->name }}</h5>
                        <p>OTP has been sent to your registered mobile number</p>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <i class="material-icons prefix pt-2">lock_outline</i>
                        <input id="password" type="text" required name="otp" maxlength="6">
                        <label for="password">OTP</label>
                        <small class="text-danger">{{ $errors->first('otp') }}</small>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <button class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12">Verify Now</button>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6 m6 l6">
                        <p class="margin medium-small"><a href="{{ route('dealer_send_otp') }}">Resend OTP?</a></p>
                    </div>
                    <div class="input-field col s6 m6 l6">
                        <p class="margin right-align medium-small"><a href="{{ route('dealer_logout') }}">Logout</a>
                        </p>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>

        $(".login-form").validate();
        setTimeout(function () {
            $(".card-alert").slideUp(300);
        }, 5000);
    </script>

@endsection
