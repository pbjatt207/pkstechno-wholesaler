@extends('layout.dealer', [
  'page_header' => 'Dealer'
])

@section('content')

    <link rel="stylesheet" type="text/css" href="{{ url('assets/vendors/dropify/css/dropify.min.css') }}">

    <script src="{{ url('assets/vendors/dropify/js/dropify.min.js') }}"></script>
    <script src="{{ url('assets/js/scripts/form-file-uploads.min.js') }}"></script>

    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>{{ $data['title'] }}</span></h5>
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('dealer_dashboard') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{ route('dealer_profile') }}">Profile</a>
                                </li>
                                <li class="breadcrumb-item active">Update
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <div class="section">

                        <!-- HTML VALIDATION  -->

                        <div class="row">
                            <div class="col s12">
                                <div id="html-validations" class="card card-tabs">
                                    <div class="card-content">
                                        <div class="card-title">
                                            <div class="row">
                                                <div class="col s12 m6 l10">
                                                    <h4 class="card-title"> Dealer Personal Information</h4>
                                                </div>
                                                <div class="col s12 m6 l2">
                                                </div>
                                            </div>
                                        </div>
                                        <div id="html-view-validations">
                                            <form enctype="multipart/form-data" class="formValidate0" id="formValidate0" method="post" action="{{ route('dealer_store') }}">
                                                @csrf
                                                <div class="row">
                                                    <div class="input-field col s6">
                                                        <label for="uname0">Name*</label>
                                                        @php $val = !empty($dealer->name) ?  $dealer->name : ''; @endphp
                                                        <input class="validate" required id="uname0" value="{{ old('name',$val) }}" name="name" type="text">
                                                        <small class="text-danger">{{ $errors->first('name') }}</small>
                                                    </div>
                                                    <div class="input-field col s6">
                                                        <label for="cemail0">E-Mail *</label>
                                                        @php $val = !empty($dealer->email) ?  $dealer->email : ''; @endphp
                                                        <input class="validate" required id="cemail0" value="{{ old('email', $val) }}" type="email" name="email">
                                                        <small class="text-danger">{{ $errors->first('email') }}</small>
                                                    </div>
                                                    <div class="input-field col s6">
                                                        <label for="mobile0">Mobile *</label>
                                                        @php $val = !empty($dealer->mobile) ?  $dealer->mobile : ''; @endphp
                                                        <input class="validate" required value="{{  old('mobile', $val) }}" id="mobile0" type="tel" name="mobile">
                                                        <small class="text-danger">{{ $errors->first('mobile') }}</small>
                                                    </div>
                                                    <div class="col s12">
                                                        @php $val = !empty($dealer->gender) ?  $dealer->gender : 'M'; @endphp
                                                        <p>Gender</p>
                                                        <p>
                                                            <label>
                                                                <input class="validate" required {{ ($val == 'M') ? "checked" : "" }} name="gender" type="radio" value="M"  />
                                                                <span>Male</span>
                                                            </label>
                                                        </p>

                                                        <label>
                                                            <input class="validate" required  {{ ($val == 'F') ? "checked" : "" }}  name="gender" value="F" type="radio" />
                                                            <span>Female</span>
                                                        </label>
                                                        <small class="text-danger">{{ $errors->first('gender') }}</small>
                                                        <div class="input-field">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="divider mb-1 mt-1"></div>
                                                <h4 class="card-title">Upload Documents</h4>
                                                <div class="row section">
                                                    <div class="col s4 m4 l4">
                                                        @php
                                                            $image = '';
                                                            if(!empty($dealer->document->pan_card)){
                                                                $image = url('/images/documents/'.$dealer->document->pan_card);
                                                            }
                                                        @endphp
                                                        <p>Pancard Image (Max 2MB).</p>
                                                        <input type="file" name="pan_card"  data-default-file="{{ $image }}" id="input-file-" class="dropify-event input-file-events" data-max-file-size="2M" />
                                                    </div>
                                                    <div class="col s4 m4 l4">
                                                        @php
                                                            $image = '';
                                                            if(!empty($dealer->document->aadhar_front)){
                                                                $image = url('/images/documents/'.$dealer->document->aadhar_front);
                                                            }
                                                        @endphp
                                                        <p>Aadhar Front (Max 2MB).</p>
                                                        <input type="file" name="aadhar_front" data-default-file="{{ $image }}" class="dropify-event input-file-events" data-max-file-size="2M" />
                                                    </div>
                                                    <div class="col s4 m4 l4">
                                                        @php
                                                            $image = '';
                                                            if(!empty($dealer->document->aadhar_back)){
                                                                $image = url('/images/documents/'.$dealer->document->aadhar_back);
                                                            }
                                                        @endphp
                                                        <p>Aadhar Back (Max 2MB).</p>
                                                        <input type="file" name="aadhar_back" data-default-file="{{ $image }}" class="dropify-event input-file-events" data-max-file-size="2M" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="input-field col s12">
                                                        <button class="btn waves-effect waves-light right" type="submit">Submit
                                                            <i class="material-icons right">send</i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div>
    <!-- END: Page Main-->
@endsection
