@extends('layout.wholesalersingle', [
  'page_header' => 'Wholesaler'
])

@section('content')

    <div id="login-page" class="row">

        <div class="col s12 m6 l4 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">
            <form class="login-form" id="" action="{{ route('wholesaler_login') }}" method="post">
                @if(Session::has('flash_message_success'))
                    <div class="card-alert card gradient-45deg-green-teal" >
                        <div class="card-content white-text">
                            {{ Session::get('flash_message_success') }}
                        </div>
                    </div>
                @endif

                @if(Session::has('flash_message_error'))
                    <div class="alert alert-danger alert-dismissable" style="margin-bottom: 0px;">
                        <div class="card-alert card gradient-45deg-red-pink" >
                            <div class="card-content white-text">
                                {{ Session::get('flash_message_error') }}
                            </div>
                        </div>
                    </div>
                @endif
                @csrf

                <div class="row">
                    <div class="input-field col s12">
                        <h5 class="ml-4">Login as Wholesaler</h5>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <i class="material-icons prefix pt-2">person_outline</i>
                        <input id="username" name="email" type="email" value="{{ old('email') }}" required autocomplete="off">
                        <label for="username" class="center-align">Email</label>
                        <small class="text-danger">{{ $errors->first('email') }}</small>
                        @error('email')
                        <div class="error">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <i class="material-icons prefix pt-2">lock_outline</i>
                        <input id="password" type="password" value="{{ old('password') }}" actocomplete="false" name="password">
                        <label for="password">Password</label>
                        <small class="text-danger">{{ $errors->first('password') }}</small>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m12 l12 ml-2 mt-1">
                        <p>
                            <label>
                                <input type="checkbox" name="remember" value="Y"/>
                                <span>Remember Me</span>
                            </label>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <button type="submit"
                                class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12">
                            Login
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6 m6 l6">
                        <p class="margin medium-small">
                            <a href="{{ route('wholesaler_forgot') }}">Forgot Password?</a>
                        </p>
                    </div>
{{--                    <div class="input-field col s6 m6 l6">--}}
{{--                        <p class="margin medium-small">--}}
{{--                            <a href="user-forgot-password.html">Forgot password ?</a>--}}
{{--                        </p>--}}
{{--                    </div>--}}
                </div>
            </form>
        </div>
    </div>
    <script>

        setTimeout(function () {
            $(".card-alert").slideUp(300);
        }, 5000);
    </script>
@endsection



