@extends('layout.dealersingle', [
  'page_header' => 'Dealer'
])

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/forgot.min.css') }}">
<div id="forgot-password" class="row">
    <div class="col s12 m6 l4 z-depth-4 offset-m4 card-panel border-radius-6 forgot-card bg-opacity-8">
        <form class="login-form" method="post" id="login-form" action="{{ route('dealer_reset', $key) }}">
            @if(Session::has('success'))
                <div class="card-alert card gradient-45deg-green-teal" >
                    <div class="card-content white-text">
                        {{ Session::get('success') }}
                    </div>
                </div>
            @endif

            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissable" style="margin-bottom: 0px;">
                    <div class="card-alert card gradient-45deg-red-pink" >
                        <div class="card-content white-text">
                            {{ Session::get('error') }}
                        </div>
                    </div>
                </div>
            @endif
            @csrf
            <div class="row">
                <div class="input-field col s12">
                    <h5 class="ml-4">Reset Password</h5>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <i class="material-icons prefix pt-2">lock_outline</i>
                    <input data-error=".errorTxt2" class="validate" id="password" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" value="{{ old('password') }}" type="password" required>
                    <label for="password" class="center-align">New Password</label>
                    <small class="errorTxt2"></small>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <i class="material-icons prefix pt-2">lock_outline</i>
                    <input data-error=".errorTxt3" class="validate" id="cppassword" name="password_confirmation" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Should be matched with new password" value="{{ old('password') }}" type="password" required>
                    <label for="cppassword" class="center-align">Confirm Password</label>
                    <small class="errorTxt3"></small>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12 mb-1">
                        Reset Password
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
    <script>
        $("#login-form").validate({
            rules: {
                otp: {
                    required: true,
                    minlength: 5
                },
                password: {
                    required: true,
                    minlength: 5
                },
                cppassword: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
            },
            //For custom messages
            messages: {
                otp: {
                    required: "Enter a OTP",
                    minlength: "Enter at least 6 characters"
                }

            },
            errorElement: 'div',
            errorPlacement: function (error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            }
        });
    </script>
@endsection
