@extends('layout.wholesaler', [
  'page_header' => 'Wholesaler'
])

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/themes/vertical-modern-menu-template/style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/noUiSlider/nouislider.min.css') }}">
    {{--    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/themes/vertical-modern-menu-template/materialize.min.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/eCommerce-products-page.min.css') }}">
    <style>
        #ecommerce-products .card-content img {
            width: 100%;
            height: 250px;
            object-fit: contain;
        }
        #ecommerce-products .card-content .btn {
            /*width: auto;*/
            /*padding: 0 1rem;*/
        }
    </style>
    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before  gradient-45deg-indigo-purple "></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>Products </span></h5>
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item ">
                                    <a href="{{ route('wholesaler_dashboard') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active">
                                    Products
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <div class="section">
                        <div class="row" id="ecommerce-products">
                            <div class="col s12 m12 l12 ">
                                @foreach($products as $key => $pro)
                                    @php

                                        $image = asset('assets/images/cards/watch-2.png');
                                        $img_path = public_path().'/images/products/'.$pro->image;

                                        if(!empty($pro->image) && file_exists($img_path)){
                                            $image = asset('images/products/'.$pro->image);
                                        }
                                    @endphp
                                <div class="col s12 m4 l4">
                                    <div class="card animate fadeLeft">
                                        @if($pro->discount_value)
                                        <div class="card-badge">
                                            <a class="white-text">
                                                <b>On Offer</b>
                                            </a>
                                        </div>
                                        @endif
                                        <div class="card-content">
{{--                                            <p>Smartwatches</p>--}}
                                            <span class="card-title text-ellipsis">{{ $pro->name }}</span>
                                            <img src="{{ $image }}" class="responsive-img" alt="">
                                            <div class="display-flex flex-wrap justify-content-center">
                                                <h5 class="mt-3">@if($pro->discount_value) <strike>&#8377; {{ $pro->price }}</strike>@endif {{ html_entity_decode("&#8377;").$pro->amount }}</h5>

                                                <a class="mt-2 btn-block waves-effect waves-light gradient-45deg-deep-purple-blue btn  z-depth-4 modal-trigger"
                                                   href="#modal{{ $pro->id }}">View</a>
                                                <a data-id="{{$pro->id}}" class=" mt-2 btn-block waves-effect waves-light gradient-45deg-purple-deep-purple btn z-depth-4 add_to_cart" >Add to cart</a>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- Modal Structure -->
                                    <div id="modal{{ $pro->id }}" class="modal">
                                        <div class="modal-content pt-2">
                                            <div class="row" id="product-one">
                                                <div class="col s12">
                                                    <a class="modal-close right"><i class="material-icons">close</i></a>
                                                </div>
                                                <div class="col m6 s12">
                                                    <img src="{{ $image }}" class="responsive-img" alt="">
                                                </div>
                                                <div class="col m6 s12">
{{--                                                    <p>Smartwatches</p>--}}
                                                    <h5>{{ $pro->name }}</h5>
{{--                                                    <span class="new badge left ml-0 mr-2" data-badge-caption="">4.2 Star</span>--}}
{{--                                                    <p>Availability: <span class="green-text">36 Item Available</span></p>--}}
                                                    <hr class="mb-5">
{{--                                                    <span class="vertical-align-top mr-4">--}}
{{--                                                        <i class="material-icons mr-3">favorite_border</i>Wishlist</span>--}}
{{--                                                    <ul class="list-bullet">--}}
{{--                                                        <li class="list-item-bullet">Accept SIM card and call</li>--}}
{{--                                                        <li class="list-item-bullet">Make calling instead of mobile phone--}}
{{--                                                        </li>--}}
{{--                                                        <li class="list-item-bullet">Sync music play and sync control--}}
{{--                                                            music--}}
{{--                                                        </li>--}}
{{--                                                        <li class="list-item-bullet">Sync Facebook,Twiter,emailand--}}
{{--                                                            calendar--}}
{{--                                                        </li>--}}
{{--                                                    </ul>--}}
                                                    <div>{{ $pro->description }}</div>
                                                    <h5>&#8377; {{ $pro->amount }}
                                                        @if(!empty($pro->discount_value))
                                                            <span class="prise-text-style ml-2">&#8377; {{ $pro->price }}</span>
                                                        @endif
                                                    </h5>
                                                    <a class="waves-effect waves-light btn gradient-45deg-deep-purple-blue mt-2 mr-2">ADD
                                                        TO CART</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                <!-- Pagination -->
{{--                                <div class="col s12 center">--}}
{{--                                    <ul class="pagination">--}}
{{--                                        <li class="disabled">--}}
{{--                                            <a href="#!">--}}
{{--                                                <i class="material-icons">chevron_left</i>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="active"><a href="#!">1</a>--}}
{{--                                        </li>--}}
{{--                                        <li class="waves-effect"><a href="#!">2</a>--}}
{{--                                        </li>--}}
{{--                                        <li class="waves-effect"><a href="#!">3</a>--}}
{{--                                        </li>--}}
{{--                                        <li class="waves-effect"><a href="#!">4</a>--}}
{{--                                        </li>--}}
{{--                                        <li class="waves-effect"><a href="#!">5</a>--}}
{{--                                        </li>--}}
{{--                                        <li class="waves-effect">--}}
{{--                                            <a href="#!">--}}
{{--                                                <i class="material-icons">chevron_right</i>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="content-overlay"></div>
            </div>
        </div>
    </div>
    <!-- END: Page Main-->

    <script src="{{ asset('assets/js/scripts/advance-ui-modals.min.js') }}"></script>
    <script src="{{ asset('assets/js/scripts/eCommerce-products-page.min.js') }}"></script>
    <script src="{{asset('assets/vendors/noUiSlider/nouislider.min.js')}}" type="text/javascript"></script>

@endsection
