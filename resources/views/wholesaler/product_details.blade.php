@extends('layout.wholesaler', [
  'page_header' => 'Wholesaler'
])

@section('content')

    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0">
                                <span>{{ $data['title'] }}</span>
                            </h5>
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('wholesaler_dashboard') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{ route('wholesaler_product') }}">{{ $data['title'] }}</a>
                                </li>
                                <li class="breadcrumb-item active">Details
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <!-- users view start -->
                    <div class="section users-view">
                        <!-- users view media object start -->
                        <div class="card-panel">
                            <div class="row">
                                <div class="col s12 m4">
                                    <div class="display-flex media">
                                        <a href="#" class="">
                                            @php $image = asset('assets/images/avatar/avatar-15.png');
                                                if(!empty($product->image)){
                                                    $image = url('images/products/'.$product->image);
                                                }
                                            @endphp
                                            <img src="{{ $image }}" alt="users view avatar" class="z-depth-4 " width="100%">
                                        </a>
                                    </div>
                                </div>
                                <div class="col s12 m5">
                                    <h5>{{ $product->name }}</h5>
                                    <h6>&#8377; {{ $product->amount }} @if(!empty($product->discount_value)) &#8377; {{ $product->price }}@endif</h6>
                                    <a class="btn gradient-45deg-purple-deep-purple" href="{{ route('wholesaler_product') }}">Send Enquiry</a>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-content">
                                <h6>Description: </h6>
                                {!! $product->description !!}
                            </div>
                        </div>

                    </div>
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div>
    <!-- END: Page Main-->
    <script>
        $(document).ready(function(){

            $(".content_section").on("click", function(){
                $(this).closest('.card-content').find('.content_data').slideToggle();
            });
            $("#plans_purchased").DataTable({
                "responsive": true,
                "Filter" : true,
                "iDisplayLength" : 10,
                "aLengthMenu" : [ [ 10, 25, 50, 100, -1 ], [ 10, 25, 50, 100, "All" ] ],
                "aaSorting" : [ [ 3, 'desc' ] ],
            });
            $("#business_groups").DataTable({
                "responsive": true,
                "Filter" : true,
                "iDisplayLength" : 10,
                "aLengthMenu" : [ [ 10, 25, 50, 100, -1 ], [ 10, 25, 50, 100, "All" ] ],
                "aaSorting" : [ [ 1, 'desc' ] ],
            });

        });
    </script>
@endsection
