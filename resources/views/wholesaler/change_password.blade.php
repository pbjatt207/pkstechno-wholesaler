@extends('layout.wholesaler', [
  'page_header' => 'Wholesaler'
])

@section('content')


    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>Change Password</span></h5>
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('wholesaler_dashboard') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active">Change Password</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <div class="section">

                        <!-- HTML VALIDATION  -->

                        <div class="row">
                            <div class="col s12">
                                <div id="html-validations" class="card card-tabs">
                                    <div class="card-content">
                                        <div class="card-title">
                                            <div class="row">
                                                <div class="col s12 m6 l10">
                                                    <h4 class="card-title">Change Password</h4>
                                                </div>
                                                <div class="col s12 m6 l2">
                                                </div>
                                            </div>
                                        </div>
                                        <div id="html-view-validations">
                                            <form class="form" id="kt_form_1" action="{{route('wholesaler_change_password')}}" method="post">
                                                @csrf

                                                @if(Session::has('success'))
                                                    <div class="card-alert card gradient-45deg-green-teal" >
                                                        <div class="card-content white-text">
                                                            {{ Session::get('success') }}
                                                        </div>
                                                    </div>
                                                @endif
                                                @if(Session::has('error'))
                                                    <div class="card-alert card gradient-45deg-red-pink" >
                                                        <div class="card-content white-text">
                                                            {{ Session::get('error') }}
                                                        </div>
                                                    </div>
                                                @endif

                                                <div class="form-group">
                                                    <label>Current Password <span class="text-danger">*</span></label>
                                                    <input type="password" class="form-control" name="current_password"/>
                                                    <small class="text-danger">{{ $errors->first('current_password') }}</small>
                                                </div>
                                                <div class="form-group">
                                                    <label>New Password <span class="text-danger">*</span></label>
                                                    <input type="password" class="form-control" name="password"/>
                                                    <small class="text-danger">{{ $errors->first('password') }}</small>
                                                </div>
                                                <div class="form-group">
                                                    <label>Confirm New Password <span class="text-danger">*</span></label>
                                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="password">
                                                    <small class="text-danger">{{ $errors->first('password_confirmation') }}</small>
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary font-weight-bold mr-2" name="submitButton">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div>
    <!-- END: Page Main-->

@endsection
