@extends('layout.wholesaler' )

@section('content')

    <link rel="stylesheet" type="text/css" href="{{ url('assets/vendors/dropify/css/dropify.min.css') }}">

    <script src="{{ url('assets/vendors/dropify/js/dropify.min.js') }}"></script>
    <script src="{{ url('assets/js/scripts/form-file-uploads.min.js') }}"></script>

    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>{{ $data['title'] }}</span></h5>
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('wholesaler_dashboard') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{ route('wholesaler_enquiry')}}">{{ $data['title'] }}</a>
                                </li>
                                <li class="breadcrumb-item active">Add
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <div class="section">

                        <!-- HTML VALIDATION  -->

                        <div class="row">
                            <div class="col s12">
                                <div id="html-validations" class="card card-tabs">
                                    <div class="card-content">
                                        <div class="card-title">
                                            <div class="row">
                                                <div class="col s12 m6 l10">
                                                    <h4 class="card-title">Product Enquiry</h4>
                                                </div>
                                                <div class="col s12 m6 l2">
                                                </div>
                                            </div>
                                        </div>
                                        <div id="html-view-validations">
                                            <form enctype="multipart/form-data" class="formValidate0" id="formValidate0" method="post" action="">
                                                @csrf
                                                <div class="row">
                                                    <div class="col s4">
                                                        <div class="input-field">
                                                            <label for="">Select Product*</label>
                                                            @php $val = !empty($info->discount) ?  $info->discount : 'amount'; @endphp
                                                            <select class="select2 browser-default" name="product">
                                                                <option value="">Select Product</option>
                                                                @foreach($products as $key => $each)
                                                                    <option value="{{ $each->id }}">{{ $each->name }}</option>
                                                                @endforeach
                                                            </select>
                                                            <small class="text-danger">{{ $errors->first('price') }}</small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="input-field col s12">
                                                        <button class="btn waves-effect waves-light right" type="submit">Submit
                                                            <i class="material-icons right">send</i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div>
    <!-- END: Page Main-->

    <script>
        $(document).ready(function(){
            $("#sender_id").on("keyup",function (){
                var sender_id = $(this).val();
                $(this).val(sender_id.toUpperCase());
                if(sender_id.length == 6){
                    $.ajax({
                        type:'POST',
                        url:"{{ route('check_sender') }}",
                        data:{sender_id :sender_id, _token: "{{ csrf_token()}}" },
                        success:function(data) {

                            if(data.status){
                                $(".sender_message").html(data.message).css('color', '#7223a2');
                            } else {
                                $(".sender_message").html(data.message).css('color', '#ff4081');
                            }
                        }
                    });
                } else {
                    $(".sender_message").html('Please Enter Unique 6 Digit Sender ID').css('color', '#6b6f82');
                }
            });
        });
    </script>
@endsection
