@extends('layout.dealersingle', [
  'page_header' => 'Dealer'
])

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/forgot.min.css') }}">
<div id="forgot-password" class="row">
    <div class="col s12 m6 l4 z-depth-4 offset-m4 card-panel border-radius-6 forgot-card bg-opacity-8">
        <form class="login-form" method="get">
            @if(Session::has('success'))
                <div class="card-alert card gradient-45deg-green-teal" >
                    <div class="card-content white-text">
                        {{ Session::get('success') }}
                    </div>
                </div>
            @endif

            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissable" style="margin-bottom: 0px;">
                    <div class="card-alert card gradient-45deg-red-pink" >
                        <div class="card-content white-text">
                            {{ Session::get('error') }}
                        </div>
                    </div>
                </div>
            @endif
            @csrf
            <div class="row">
                <div class="input-field col s12">
                    <h5 class="ml-4">Forgot Password</h5>
                    <p class="ml-4">You can reset your password</p>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <i class="material-icons prefix pt-2">person_outline</i>
                    <input class="validate" id="email" name="email" value="{{ old('email') }}" type="email" required>
                    <label for="email" class="center-align">Email</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12 mb-1">
                        Reset Password
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6 m6 l6">
                    <p class="margin medium-small">
                        <a href="{{ url('msg-dealer') }}">Login</a>
                    </p>
                </div>
{{--                <div class="input-field col s6 m6 l6">--}}
{{--                    <p class="margin right-align medium-small"><a href="user-register.html">Register</a></p>--}}
{{--                </div>--}}
            </div>
        </form>
    </div>
</div>
    <script>
        $(".login-form").validate();
    </script>
@endsection
