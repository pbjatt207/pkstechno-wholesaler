@extends('layout.wholesaler', [
  'page_header' => 'Wholesaler'
])

@section('content')

    <link rel="stylesheet" type="text/css" href="{{ url('assets/vendors/dropify/css/dropify.min.css') }}">

    <script src="{{ url('assets/vendors/dropify/js/dropify.min.js') }}"></script>
    <script src="{{ url('assets/js/scripts/form-file-uploads.min.js') }}"></script>

    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>{{ $data['title'] }}</span></h5>
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('wholesaler_dashboard') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{ route('wholesaler_profile') }}">Profile</a>
                                </li>
                                <li class="breadcrumb-item active">Update
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <div class="section">

                        <!-- HTML VALIDATION  -->

                        <div class="row">
                            <div class="col s12">
                                <div id="html-validations" class="card card-tabs">
                                    <div class="card-content">
                                        <div class="card-title">
                                            <div class="row">
                                                <div class="col s12 m6 l10">
                                                    <h4 class="card-title">Personal Information</h4>
                                                </div>
                                                <div class="col s12 m6 l2">
                                                </div>
                                            </div>
                                        </div>
                                        <div id="html-view-validations">
                                            <form enctype="multipart/form-data" class="formValidate0" id="formValidate0" method="post" action="{{ route('wholesaler_store') }}">
                                                @csrf
                                                <div class="row">
                                                    <div class="input-field col s6">
                                                        <label for="uname0">Name*</label>
                                                        @php $val = !empty($info->name) ?  $info->name : ''; @endphp
                                                        <input class="validate" required id="uname0" value="{{ old('name',$val) }}" name="name" type="text">
                                                        <small class="text-danger">{{ $errors->first('name') }}</small>
                                                    </div>
                                                    <div class="input-field col s6">
                                                        <label for="cemail0">E-Mail *</label>
                                                        @php $val = !empty($info->email) ?  $info->email : ''; @endphp
                                                        <input class="validate" required id="cemail0" value="{{ old('email', $val) }}" type="email" name="email">
                                                        <small class="text-danger">{{ $errors->first('email') }}</small>
                                                    </div>
                                                    <div class="input-field col s6">
                                                        <label for="mobile0">Mobile *</label>
                                                        @php $val = !empty($info->mobile) ?  $info->mobile : ''; @endphp
                                                        <input class="validate" required value="{{  old('mobile', $val) }}" id="mobile0" type="tel" name="mobile">
                                                        <small class="text-danger">{{ $errors->first('mobile') }}</small>
                                                    </div>
                                                    <div class="input-field col s6">
                                                        <label for="shop">Shop Name *</label>
                                                        @php $val = !empty($info->shop) ?  $info->shop : ''; @endphp
                                                        <input class="validate" required value="{{  old('shop', $val) }}" id="shop" type="text" name="shop">
                                                        <small class="text-danger">{{ $errors->first('shop') }}</small>
                                                    </div>
                                                    <div class="input-field col s12">
                                                        <label for="address">Address *</label>
                                                        @php $val = !empty($info->address) ?  $info->address : ''; @endphp
                                                        <input class="validate" required value="{{  old('address', $val) }}" id="address" type="text" name="address">
                                                        <small class="text-danger">{{ $errors->first('address') }}</small>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="input-field col s12">
                                                        <button class="btn waves-effect waves-light right" type="submit">Update
                                                            <i class="material-icons right">send</i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div>
    <!-- END: Page Main-->
@endsection
