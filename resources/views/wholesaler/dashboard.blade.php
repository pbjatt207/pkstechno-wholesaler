@extends('layout.wholesaler', [
  'page_header' => 'Wholesaler'
])

@section('content')
{{--        <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/chartist-js/chartist.min.css') }}">--}}
{{--        <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/chartist-js/chartist-plugin-tooltip.css') }}">--}}

{{--    <script src="{{asset('assets/js/scripts/dashboard-modern.js')}}" type="text/javascript"></script>--}}
{{--    <script src="{{ asset('assets/vendors/chartjs/chart.min.js') }}"></script>--}}
{{--    <script src="{{ asset('assets/vendors/chartist-js/chartist.min.js') }}"></script>--}}
{{--    <script src="{{ asset('assets/vendors/chartist-js/chartist-plugin-tooltip.js') }}"></script>--}}
{{--    <script src="{{ asset('assets/vendors/chartist-js/chartist-plugin-fill-donut.min.js') }}"></script>--}}

    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="col s12">
                <div class="container">
                    @if(Session::has('success'))
                        <div class="card-alert card gradient-45deg-green-teal" >
                            <div class="card-content white-text">
                                {{ Session::get('success') }}
                            </div>
                        </div>
                    @endif
                    @if(Session::has('error'))
                        <div class="card-alert card gradient-45deg-red-pink" >
                            <div class="card-content white-text">
                                {{ Session::get('error') }}
                            </div>
                        </div>
                    @endif
                    <div class="section">
                        <!-- Current balance & total transactions cards-->
                        <div class="row vertical-modern-dashboard">
                            <div class="col s12 m4 l4">
                                <!-- Current Balance -->
{{--                                <div class="card animate fadeLeft">--}}
{{--                                    <div class="card-content">--}}
{{--                                        <h6 class="mb-0 mt-0 display-flex justify-content-between">Total Commission <i--}}
{{--                                                class="material-icons float-right">more_vert</i>--}}
{{--                                        </h6>--}}
{{--                                        <div class="current-balance-container">--}}
{{--                                            <div id="current-balance-donut-chart" class="current-balance-shadow"></div>--}}
{{--                                        </div>--}}
{{--                                        <h5 class="center-align">&#8377; {{ $commission }}</h5>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div class="card padding-4 animate fadeLeft">
                                    <a href="{{ route('wholesaler_enquiry') }}" class="grey-text">
                                        <div class="row">
                                            <div class="col s5 m5">
                                                <h5 class="mb-0">{{ $enquiry_products }}</h5>
    {{--                                            <p class="no-margin">New</p>--}}
    {{--                                            <p class="mb-0 pt-8">{{ $total_businesses }}</p>--}}
                                            </div>
                                            <div class="col s7 m7 right-align">
                                                <i
                                                    class="material-icons background-round mt-5 mb-5 gradient-45deg-purple-amber gradient-shadow white-text">message</i>
                                                <p class="mb-0">Total Enquiry</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col s12 m6 l4">
                                <div class="card padding-4 animate fadeLeft">
                                    <a href="{{ route('wholesaler_product') }}" class="grey-text">
                                        <div class="row">
                                            <div class="col s5 m5">
                                                <h5 class="mb-0">{{ @$products }}</h5>
                                                {{--                                            <p class="no-margin">New</p>--}}
                                                {{--                                            <p class="mb-0 pt-8">{{ @$total_businesses }}</p>--}}
                                            </div>
                                            <div class="col s7 m7 right-align">
                                                <i
                                                    class="material-icons background-round mt-5 mb-5 gradient-45deg-purple-amber gradient-shadow white-text">shopping_cart</i>
                                                <p class="mb-0">Vendor Products</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
{{--                                <div id="chartjs" class="card pt-0 pb-0 animate fadeLeft">--}}
{{--                                    <div class="dashboard-revenue-wrapper padding-2 ml-2">--}}
{{--                                        <span class="new badge gradient-45deg-indigo-purple gradient-shadow mt-2 mr-2">+ $900</span>--}}
{{--                                        <p class="mt-2 mb-0 font-weight-600">Today's revenue</p>--}}
{{--                                        <p class="no-margin grey-text lighten-3">$40,512 avg</p>--}}
{{--                                        <h5>$ 22,300</h5>--}}
{{--                                    </div>--}}
{{--                                    <div class="sample-chart-wrapper card-gradient-chart">--}}
{{--                                        <canvas id="custom-line-chart-sample-three" class="center"></canvas>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            </div>
                        </div>
                        <!--/ Current balance & total transactions cards-->

                    </div>
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div>
    <!-- END: Page Main-->

@endsection
