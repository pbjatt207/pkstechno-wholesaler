@extends('layout.wholesaler', [
  'page_header' => 'Wholesaler'
])

@section('content')

    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>{{ $data['title'] }}</span></h5>
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('wholesaler_dashboard') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="#">{{ $data['title'] }}</a>
                                </li>
                                <li class="breadcrumb-item active">List
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <!-- users list start -->
                    <section class="users-list-wrapper section">

                        <div class="users-list-table">
                            <form action="" id="product_from" method="post">
                                @csrf
                                <div class="card">
                                    <div class="card-content">
                                        <!-- datatable start -->
                                        @if(Session::has('success'))
                                            <div class="card-alert card green">
                                                <div class="card-content white-text">
                                                    {{Session::get('success')}}
                                                </div>
                                            </div>
                                        @endif
                                        <div class=""><button type="button" class="float-right btn btn-small send_enquiry">Send Enquiry</button></div>
                                        <div class="responsive-table">
                                            <table id="table" class="table" width="100%">
                                                <thead>
                                                <tr>

                                                    <th>
                                                        <p>
                                                            <label>
                                                                <input type="checkbox" class="filled-in check_all" />
                                                                <span></span>
                                                            </label>
                                                        </p>
                                                    </th>
                                                    <th>Image</th>
                                                    <th>Name</th>
                                                    <th>Amount</th>
                                                    <th>Quantity</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($products as $key => $each)
                                                    @php
                                                        $image = asset('assets/images/cards/watch-2.png');
                                                        $img_path = public_path().'/images/products/'.$each->image;

                                                        if(!empty($each->image) && file_exists($img_path)){
                                                            $image = asset('images/products/'.$each->image);
                                                        }
                                                    @endphp
                                                    <tr>

                                                        <td>
                                                            <p>
                                                                <label>
                                                                    <input type="checkbox" class="filled-in check" name="product[]" value="{{ $each->id }}" />
                                                                    <span></span>
                                                                </label>
                                                            </p>
                                                        </td>
                                                        <td>
                                                            <img src="{{ $image  }}" width="80px"></td>
                                                        <td>
                                                            <a href="{{ route('wholesaler_product_details', $each->id) }}">{{ $each->name }}</a>
                                                        </td>
                                                        <td>&#8377; {{ $each->amount  }}</td>
                                                        <td>
                                                            <div>
                                                                <button type="button" class="button btn-floating waves-effect waves-light green darken-1">-</button>
                                                                <input type="number" name="quantity[{{ $each->id }}]" class="input" value="1" min="1" />
                                                                <button type="button" class="button btn-floating waves-effect waves-light green darken-1">+</button>
                                                            </div>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="">
                                            <button type="button" class="float-right btn btn-small send_enquiry">Send Enquiry</button>
                                            <div class="clearfix"></div>
                                        </div>
                                        <!-- datatable ends -->
                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>
                    <!-- users list ends -->

{{--                    <div style="bottom: 50px; right: 19px;" class="fixed-action-btn direction-top">--}}
{{--                        <a href="{{ route('wholesaler_enquiry_create') }}" class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow">--}}
{{--                            <i class="material-icons">add</i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div>
    <!-- END: Page Main-->
    <link href="https://cdn.datatables.net/colreorder/1.2.0/css/colReorder.dataTables.min.css">
    <script src="https://cdn.datatables.net/colreorder/1.2.0/js/dataTables.colReorder.min.js"></script>
    <script>
        $(document).ready(function() {
            $(".send_enquiry").on("click", function () {
                var checkbox = $(".check:checkbox");
                var checked = $(".check:checkbox:checked").length;
                console.log(checked);
                if(checked >= 1){
                    swal({
                        title: "Are you sure?",
                        text: "Do you want to send enquiry!",
                        icon: 'warning',
                        dangerMode: true,
                        buttons: {
                            cancel: 'No',
                            delete: 'Yes'
                        }
                    }).then(function (willDelete) {
                        if (willDelete) {
                            $("#product_from").submit();
                        }
                    });

                } else {
                    M.toast({html: "Please select at least one product to send enquiry", classes: 'rounded'});
                }
            });
            $(".button").on("click", function() {
                var $button = $(this);
                var $parent = $button.parent();
                var oldValue = $parent.find('.input').val();

                if ($button.text() == "+") {
                    var newVal = parseFloat(oldValue) + 1;
                } else {
                    // Don't allow decrementing below zero
                    if (oldValue > 1) {
                        var newVal = parseFloat(oldValue) - 1;
                    } else {
                        newVal = 1;
                    }
                }
                $parent.find('a.add-to-cart').attr('data-quantity', newVal);
                $parent.find('.input').val(newVal);
            });
            $("#table").DataTable({
                // responsive : true,
                filter: false,
                processing: false,
                bPaginate : false,
                dom: '  ',
                "responsive": {
                    details: {
                        type: 'column',
                        target: -1
                    }
                },
                "columnDefs": [
                    {
                        className: 'control',
                        orderable: false,
                        targets:   -1
                    },
                    { responsivePriority: 1, targets: 0 },
                    { responsivePriority: 2, targets: 4 },
                    { responsivePriority: 3, targets: 2 }
                ],
                // colReorder: {
                //     order: [0,1,3]
                // }

            });
            $('#tabdle').DataTable({
                "responsive": false,
                "processing" : false,
                "Filter" : true,
                "serverSide" : false,
                "sServerMethod" : "POST",
                "ajax":{
                    "url": "{{ route('wholesaler_product_grid') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{ csrf_token()}}"}
                },
                "iDisplayLength" : 10,
                //"aLengthMenu" : [ [ 10, 25, 50, 100, -1 ], [ 10, 25, 50, 100, "All" ] ],
                "aaSorting" : [ [ 0, 'desc' ] ],
                "aoColumnDefs": [
                    { "aTargets": [ 4 ], "orderable": false, "searchable" : false , "sClass": "text-center" }
                ],
                "columns": [
                    { "data": "id" },
                    { "data": "image" },
                    { "data": "name" },
                    { "data": "amount" },
                    { "data": "input" },
                ],

                // "fnServerParams": function ( aoData ) {
                //     aoData.push(
                //         { "name": "csrf_test_name", "value": $.cookie("csrf_cookie_name") }
                //     );
                // },
                "dom": 'Tfg<"search_box pull-right dt_search_box"><"pull-left text-right report_table_length"l>t<"pull-left"i><"pull-right"p>',
                colReorder: {
                    order: [0,1,4]
                }
            });
        } );
    </script>
@endsection
