<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Wholesale Login | {{ env('APP_NAME') }}</title>
    <link rel="apple-touch-icon" href="../../../app-assets/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="../../../app-assets/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/css/themes/vertical-modern-menu-template/materialize.min.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/css/themes/vertical-modern-menu-template/style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/login.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/custom/custom.css') }}">
    <script src="{{asset('assets/js/vendors.min.js')}}" type="text/javascript"></script>
</head>
<!-- END: Head-->
<body
    class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 1-column login-bg   blank-page blank-page"
    data-open="click" data-menu="vertical-modern-menu" data-col="1-column">
<div class="row">
    <div class="col s12">
        <div class="container">
            @yield('content')
{{--            <div id="login-page" class="row">--}}
{{--                <div class="col s12 m6 l4 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">--}}
{{--                    <form class="login-form" id="" action="{{ route('admin_login') }}" method="post">--}}
{{--                        @csrf--}}
{{--                        <div class="row">--}}
{{--                            <div class="input-field col s12">--}}
{{--                                <h5 class="ml-4">Sign in</h5>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row margin">--}}
{{--                            <div class="input-field col s12">--}}
{{--                                <i class="material-icons prefix pt-2">person_outline</i>--}}
{{--                                <input id="username" name="email" type="email" value="{{ old('email') }}" required autocomplete="off">--}}
{{--                                <label for="username" class="center-align">Email</label>--}}
{{--                                <small class="text-danger">{{ $errors->first('email') }}</small>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row margin">--}}
{{--                            <div class="input-field col s12">--}}
{{--                                <i class="material-icons prefix pt-2">lock_outline</i>--}}
{{--                                <input id="password" type="password" value="{{ old('password') }}" actocomplete="false" name="password">--}}
{{--                                <label for="password">Password</label>--}}
{{--                                <small class="text-danger">{{ $errors->first('password') }}</small>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row">--}}
{{--                            <div class="col s12 m12 l12 ml-2 mt-1">--}}
{{--                                <p>--}}
{{--                                    <label>--}}
{{--                                        <input type="checkbox" name="remember" value="Y"/>--}}
{{--                                        <span>Remember Me</span>--}}
{{--                                    </label>--}}
{{--                                </p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row">--}}
{{--                            <div class="input-field col s12">--}}
{{--                                <button type="submit"--}}
{{--                                        class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12">--}}
{{--                                    Login--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row">--}}
{{--                            --}}{{--                <div class="input-field col s6 m6 l6">--}}
{{--                            --}}{{--                  <p class="margin medium-small"><a href="user-register.html">Register Now!</a></p>--}}
{{--                            --}}{{--                </div>--}}
{{--                            <div class="input-field col s6 m6 l6">--}}
{{--                                <p class="margin medium-small"><a href="user-forgot-password.html">Forgot password ?</a>--}}
{{--                                </p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
        <div class="content-overlay"></div>
    </div>
</div>

<script src="{{asset('assets/js/plugins.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/search.min.js')}}"></script>
<script src="{{asset('assets/vendors/jquery-validation/jquery.validate.min.js')}}" type="text/javascript"></script>
{{--<script src="{{asset('assets/js/custom/custom-script.min.js')}}" type="text/javascript"></script>--}}
{{--<script src="{{asset('assets/js/scripts/form-validation.js')}}" type="text/javascript"></script>--}}
<script>

    setTimeout(function () {
        $(".card-alert").slideUp(300);
    }, 5000);
</script>
</body>

<!-- Mirrored from pixinvent.com/materialize-material-design-admin-template/html/ltr/vertical-modern-menu-template/user-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 28 Nov 2020 06:19:46 GMT -->
</html>
