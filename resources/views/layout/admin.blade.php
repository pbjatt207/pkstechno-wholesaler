@php

@endphp

    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

    <title>Admin Dashboard | {{ env('APP_NAME') }}</title>

    <link rel="apple-touch-icon" href="{{asset('assets/images/favicon/apple-touch-icon-152x152.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/images/favicon/favicon-32x32.png') }}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/animate-css/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/chartist-js/chartist.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/chartist-js/chartist-plugin-tooltip.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/flag-icon/css/flag-icon.min.css') }}">

    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/css/themes/vertical-modern-menu-template/materialize.min.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/css/themes/vertical-modern-menu-template/style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/dashboard-modern.css') }}">
    {{--    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/intro.min.css') }}">--}}
    <link href="{{ asset('assets/vendors/sweetalert/sweetalert.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/data-tables/css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/page-users.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/custom/custom.css') }}">


    <link rel="stylesheet" href="{{asset('assets/vendors/select2/select2.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/vendors/select2/select2-materialize.css') }}" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/form-select2.min.css') }}">

    <script src="{{asset('assets/js/vendors.min.js')}}" type="text/javascript"></script>
</head>
<body
    class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   "
    data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">


<!-- BEGIN: Header-->
<header class="page-topbar" id="header">
    <div class="navbar navbar-fixed">
        <nav
            class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark gradient-45deg-indigo-purple no-shadow">
            <div class="nav-wrapper">

                {{--            <div class="header-search-wrapper hide-on-med-and-down"><i class="material-icons">search</i>--}}
                {{--              <input class="header-search-input z-depth-2" type="text" name="Search" placeholder="Explore Materialize" data-search="template-list">--}}
                {{--              <ul class="search-list collection display-none"></ul>--}}
                {{--            </div>--}}
                <ul class="navbar-list right">

                    <li class="hide-on-med-and-down"><a class="waves-effect waves-block waves-light toggle-fullscreen"
                                                        href="javascript:void(0);"><i class="material-icons">settings_overscan</i></a>
                    </li>
{{--                    <li class="hide-on-large-only search-input-wrapper"><a--}}
{{--                            class="waves-effect waves-block waves-light search-button" href="javascript:void(0);"><i--}}
{{--                                class="material-icons">search</i></a></li>--}}
{{--                    <li>--}}
{{--                        <a class="waves-effect waves-block waves-light notification-button" href="javascript:void(0);"--}}
{{--                           data-target="notifications-dropdown">--}}
{{--                            <i class="material-icons">notifications_none--}}
{{--                                @if(COUNT($notifications) > 0)<small class="notification-badge">{{ $unread }}</small>@endif--}}
{{--                            </i>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                    <li>
                        <a class="waves-effect waves-block waves-light" href="{{ route('admin_logout') }}">
                            <i class="material-icons">logout</i>
                        </a>
                    </li>
{{--                    <li>--}}
{{--                        <a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);"--}}
{{--                           data-target="profile-dropdown">--}}
{{--                          <span class="avatar-status avatar-online">--}}
{{--                              <img src="{{ asset('assets/images/avatar/avatar-7.png') }}" alt="avatar">--}}
{{--                              <i></i>--}}
{{--                          </span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                    {{--              <li><a class="waves-effect waves-block waves-light sidenav-trigger" href="#" data-target="slide-out-right"><i class="material-icons">format_indent_increase</i></a></li>--}}
                </ul>
                @if(@$notifications)
                <!-- notifications-dropdown-->
                <ul class="dropdown-content" id="notifications-dropdown">
                    <li onclick="location.href= '{{ route("notifications") }}'">
                        <h6>NOTIFICATIONS<span class="new badge">{{ $unread }}</span></h6>
                    </li>
                    <li class="divider"></li>
                    @foreach($notifications as $key => $each)
                    <li>
                        <a class="black-text" href="#!">
                            <span class="material-icons icon-bg-circle cyan small">settings</span>
                            {{ $each->message }}
                        </a>
                        <time class="media-meta grey-text darken-2" datetime="">{{ Carbon\Carbon::parse($each->created_at)->diffForHumans() }}</time>
                    </li>
                    @endforeach
{{--                    <li><a class="black-text" href="#!"><span--}}
{{--                                class="material-icons icon-bg-circle red small">stars</span> Completed the task</a>--}}
{{--                        <time class="media-meta grey-text darken-2" datetime="2015-06-12T20:50:48+08:00">3 days ago--}}
{{--                        </time>--}}
{{--                    </li>--}}
{{--                    <li><a class="black-text" href="#!"><span--}}
{{--                                class="material-icons icon-bg-circle teal small">settings</span> Settings updated</a>--}}
{{--                        <time class="media-meta grey-text darken-2" datetime="2015-06-12T20:50:48+08:00">4 days ago--}}
{{--                        </time>--}}
{{--                    </li>--}}
{{--                    <li><a class="black-text" href="#!"><span class="material-icons icon-bg-circle deep-orange small">today</span>--}}
{{--                            Director meeting started</a>--}}
{{--                        <time class="media-meta grey-text darken-2" datetime="2015-06-12T20:50:48+08:00">6 days ago--}}
{{--                        </time>--}}
{{--                    </li>--}}
{{--                    <li><a class="black-text" href="#!"><span class="material-icons icon-bg-circle amber small">trending_up</span>--}}
{{--                            Generate monthly report</a>--}}
{{--                        <time class="media-meta grey-text darken-2" datetime="2015-06-12T20:50:48+08:00">1 week ago--}}
{{--                        </time>--}}
{{--                    </li>--}}
                </ul>
                @endif
                <!-- profile-dropdown-->
{{--                <ul class="dropdown-content" id="profile-dropdown">--}}
{{--                    <li><a class="grey-text text-darken-1" href="">--}}
{{--                            <i class="material-icons">person_outline</i>--}}
{{--                            Settings</a></li>--}}
{{--                    <li><a class="grey-text text-darken-1" href="{{ route('admin_logout') }}"><i class="material-icons">keyboard_tab</i>--}}
{{--                            Logout</a></li>--}}
{{--                </ul>--}}
            </div>
        </nav>
    </div>
</header>
<!-- END: Header-->


<!-- BEGIN: SideNav-->
<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square ">
    <div class="brand-sidebar">
        <h1 class="logo-wrapper">
            <a class="brand-logo darken-1" href="{{ url('admin') }}">
                <img class="hide-on-med-and-down" src="{{ asset('assets/images/logo/materialize-logo-color.png') }}"/>
                <img class="show-on-medium-and-down hide-on-med-and-up"
                     src="{{ asset('assets/images/logo/materialize-logo.png') }}"/>
                <span class="logo-text hide-on-med-and-down">{{ env('APP_NAME') }}</span>
            </a>
            <a class="navbar-toggler" href="#">
                <i class="material-icons">radio_button_checked</i>
            </a>
        </h1>
    </div>
    <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out"
        data-menu="menu-navigation" data-collapsible="menu-accordion">
        <li class="active bold">
            <a class=" waves-effect waves-cyan @if($data['menu'] == 'dashboard') active @endif"
               href="{{ url('admin/dashboard') }}">
                <i class="material-icons">settings_input_svideo</i>
                <span class="menu-title" data-i18n="Dashboard">Dashboard</span>
                {{--                <span class="badge badge pill orange float-right mr-10">3</span>--}}
            </a>
        </li>
        <li class="bold">
            <a class="waves-effect waves-cyan @if($data['menu'] == 'vendors') active @endif"
               href="{{ route('admin_vendor') }}">
                <i class="material-icons">persons_outline</i>
                <span class="menu-title" data-i18n="Mail">Vendors</span>
                {{--                <span class="badge badge pill purple float-right mr-10">3</span>--}}
            </a>
        </li>
        <li class="bold">
            <a class="waves-effect waves-cyan @if($data['menu'] == 'wholesaler') active @endif"
               href="{{ route('admin_wholesaler') }}">
                <i class="material-icons">persons_outline</i>
                <span class="menu-title" data-i18n="Mail">Wholesaler</span>
{{--                <span class="badge badge pill purple float-right mr-10">3</span>--}}
            </a>
        </li>
{{--        <li class="bold">--}}
{{--            <a class="waves-effect waves-cyan @if($data['menu'] == 'retailers') active @endif"--}}
{{--               href="{{ route('admin_retailer') }}">--}}
{{--                <i class="material-icons">persons_outline</i>--}}
{{--                <span class="menu-title" data-i18n="Mail">Retailer</span>--}}
{{--                --}}{{--                <span class="badge badge pill purple float-right mr-10">3</span>--}}
{{--            </a>--}}
{{--        </li>--}}
        <li class="bold">
            <a class="waves-effect waves-cyan @if($data['menu'] == 'products') active @endif"
               href="{{ route('product') }}">
                <i class="material-icons">shopping_cart</i>
                <span class="menu-title" data-i18n="Mail">Products</span>
                {{--                <span class="badge badge pill purple float-right mr-10">3</span>--}}
            </a>
        </li>
        <li class="bold">
            <a class="waves-effect waves-cyan @if($data['menu'] == 'enquiry') active @endif"
               href="{{ route('enquiry') }}">
                <i class="material-icons">message</i>
                <span class="menu-title" data-i18n="Mail">Enquiry</span>
                {{--                <span class="badge badge pill purple float-right mr-10">3</span>--}}
            </a>
        </li>
{{--        <li class="bold">--}}
{{--            <a class="waves-effect waves-cyan @if($data['menu'] == 'plans') active @endif"--}}
{{--               href="{{ url('admin/plans') }}">--}}
{{--                <i class="material-icons">receipt</i>--}}
{{--                <span class="menu-title" data-i18n="Mail">Plans</span>--}}
{{--                --}}{{--                <span class="badge badge pill purple float-right mr-10">3</span>--}}
{{--            </a>--}}
{{--        </li>--}}
{{--        <li class="bold">--}}
{{--            <a class="waves-effect waves-cyan @if($data['menu'] == 'contacts') active @endif"--}}
{{--               href="{{ route('contacts') }}">--}}
{{--                <i class="material-icons">import_contacts</i>--}}
{{--                <span class="menu-title" data-i18n="Contacts">Contacts</span>--}}
{{--            </a>--}}
{{--        </li>--}}

{{--        <li class="navigation-header">--}}
{{--            <a class="navigation-header-text">Pages </a>--}}
{{--            <i class="navigation-header-icon material-icons">more_horiz</i>--}}
{{--        </li>--}}
{{--        <li class="bold">--}}
{{--            <a class="waves-effect waves-cyan @if($data['menu'] == 'plans') active @endif"--}}
{{--               href="{{ url('admin/plans') }}">--}}
{{--                <i class="material-icons">person_outline</i>--}}
{{--                <span class="menu-title" data-i18n="User Profile">User Profile</span>--}}
{{--            </a>--}}
{{--        </li>--}}
{{--        <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="JavaScript:void(0)"><i--}}
{{--                    class="material-icons">content_paste</i><span class="menu-title" data-i18n="Pages">Pages</span></a>--}}
{{--            <div class="collapsible-body">--}}
{{--                <ul class="collapsible collapsible-sub" data-collapsible="accordion">--}}
{{--                    <li><a href="page-contact.html"><i class="material-icons">radio_button_unchecked</i><span--}}
{{--                                data-i18n="Contact">Contact</span></a>--}}
{{--                    </li>--}}
{{--                    <li><a href="page-blog-list.html"><i class="material-icons">radio_button_unchecked</i><span--}}
{{--                                data-i18n="Blog">Blog</span></a>--}}
{{--                    </li>--}}
{{--                    <li><a href="page-search.html"><i class="material-icons">radio_button_unchecked</i><span--}}
{{--                                data-i18n="Search">Search</span></a>--}}
{{--                    </li>--}}
{{--                    <li><a href="page-knowledge.html"><i class="material-icons">radio_button_unchecked</i><span--}}
{{--                                data-i18n="Knowledge">Knowledge</span></a>--}}
{{--                    </li>--}}
{{--                    <li><a href="page-timeline.html"><i class="material-icons">radio_button_unchecked</i><span--}}
{{--                                data-i18n="Timeline">Timeline</span></a>--}}
{{--                    </li>--}}
{{--                    <li><a href="page-faq.html"><i class="material-icons">radio_button_unchecked</i><span--}}
{{--                                data-i18n="FAQs">FAQs</span></a>--}}
{{--                    </li>--}}
{{--                    <li><a href="page-account-settings.html"><i class="material-icons">radio_button_unchecked</i><span--}}
{{--                                data-i18n="Account Settings">Account Settings</span></a>--}}
{{--                    </li>--}}
{{--                    <li><a href="page-blank.html"><i class="material-icons">radio_button_unchecked</i><span--}}
{{--                                data-i18n="Page Blank">Page Blank</span></a>--}}
{{--                    </li>--}}
{{--                    <li><a href="page-collapse.html"><i class="material-icons">radio_button_unchecked</i><span--}}
{{--                                data-i18n="Page Collapse">Page Collapse</span></a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        </li>--}}
    </ul>
    <div class="navigation-background"></div>
    <a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only"
       href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
</aside>
<!-- END: SideNav-->

<!-- BEGIN: Page Main-->
@yield('content')
<!-- END: Page Main-->


<!-- BEGIN: Footer-->
<footer
    class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
    <div class="footer-copyright">
        <div class="container">
            <span>&copy; 2020
                <a href="#" target="_blank">MESSAGE APP</a>
                All rights reserved.
            </span>
            <span class="right hide-on-small-only">Design and Developed by
                <a href="#">SUNCITY TECHNO</a>
            </span>
        </div>
    </div>
</footer>


<script src="{{asset('assets/js/plugins.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/search.min.js')}}"></script>
<script src="{{asset('assets/js/scripts/dashboard-modern.min.js')}}" type="text/javascript"></script>
{{--    <script src="{{asset('assets/js/scripts/intro.min.js')}}" type="text/javascript"></script>--}}
<script src="{{asset('assets/vendors/data-tables/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js')}}"
        type="text/javascript"></script>

<script src="{{asset('assets/js/custom/custom-script.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/scripts/customizer.min.js')}}" type="text/javascript"></script>

<script src="{{asset('assets/js/scripts/page-users.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/jquery-validation/jquery.validate.min.js')}}" type="text/javascript"></script>

<script src="{{ asset('assets/vendors/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/vendors/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/js/scripts/form-select2.min.js') }}"></script>
<script src="{{ asset('assets/js/scripts/advance-ui-toasts.js') }}"></script>


</body>
</html>
`
