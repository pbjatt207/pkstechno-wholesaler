@php

    $userinfo = \Illuminate\Support\Facades\Auth::guard('wholesaler')->user();

@endphp

    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description"
          content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google.">
    <meta name="keywords"
          content="materialize, admin template, dashboard template, flat admin template, responsive admin template, eCommerce dashboard, analytic dashboard">
    <meta name="author" content="ThemeSelect">

    <title>Welcome to the Wholesaler Dashboard | {{ env('APP_NAME') }}</title>

    <link rel="apple-touch-icon" href="{{asset('assets/images/favicon/apple-touch-icon-152x152.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/images/favicon/favicon-32x32.png') }}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/animate-css/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/flag-icon/css/flag-icon.min.css') }}">

    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/css/themes/vertical-modern-menu-template/materialize.min.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/css/themes/vertical-modern-menu-template/style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/dashboard-modern.css') }}">
    {{--    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/intro.min.css') }}">--}}
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/data-tables/css/jquery.dataTables.min.css') }}">


    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css') }}">

    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/css/themes/vertical-modern-menu-template/materialize.min.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/css/themes/vertical-modern-menu-template/style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/page-users.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/custom/custom.css') }}">


    <link rel="stylesheet" href="{{asset('assets/vendors/select2/select2.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/vendors/select2/select2-materialize.css') }}" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/form-select2.min.css') }}">
    <link href="{{ asset('assets/vendors/sweetalert/sweetalert.css') }}">

    <script src="{{asset('assets/js/vendors.min.js')}}" type="text/javascript"></script>
    @if($userinfo->is_verified == 'N')
        <script>
            window.location.href = '{{ route('wholesaler_verify') }}'
        </script>
    @endif
</head>
<body
    class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   "
    data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">


<!-- BEGIN: Header-->
<header class="page-topbar" id="header">
    <div class="navbar navbar-fixed">
        <nav
            class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark gradient-45deg-indigo-purple no-shadow">
            <div class="nav-wrapper">

                {{--            <div class="header-search-wrapper hide-on-med-and-down"><i class="material-icons">search</i>--}}
                {{--              <input class="header-search-input z-depth-2" type="text" name="Search" placeholder="Explore Materialize" data-search="template-list">--}}
                {{--              <ul class="search-list collection display-none"></ul>--}}
                {{--            </div>--}}
                <ul class="navbar-list right">

                    <li class="hide-on-med-and-down"><a class="waves-effect waves-block waves-light toggle-fullscreen"
                                                        href="javascript:void(0);"><i class="material-icons">settings_overscan</i></a>
                    </li>
{{--                    <li class="hide-on-large-only search-input-wrapper">--}}
{{--                        <a--}}
{{--                            class="waves-effect waves-block waves-light search-button" href="javascript:void(0);"><i--}}
{{--                                class="material-icons">search</i></a>--}}
{{--                    </li>--}}
                    <li>
                        <a class="waves-effect waves-block waves-light profile-button" href="javascript:void()"
                           data-target="profile-dropdown">
                            <i class="material-icons">logout</i>
                        </a>
                    </li>
                </ul>
                <!-- profile-dropdown-->
                <ul class="dropdown-content" id="profile-dropdown">
                    <li>
                        <a class="grey-text text-darken-1" href="{{ route('wholesaler_logout') }}">
                            <i class="material-icons">keyboard_tab</i>
                            Logout
                        </a>
                    </li>
                </ul>
            </div>
            <nav class="display-none search-sm">
                <div class="nav-wrapper">
                    <form id="navbarForm">
                        <div class="input-field search-input-sm">
                            <input class="search-box-sm mb-0" type="search" required="" id="search"
                                   placeholder="Explore Materialize" data-search="template-list">
                            <label class="label-icon" for="search"><i
                                    class="material-icons search-sm-icon">search</i></label><i
                                class="material-icons search-sm-close">close</i>
                            <ul class="search-list collection search-list-sm display-none"></ul>
                        </div>
                    </form>
                </div>
            </nav>
        </nav>
    </div>
</header>


<!-- BEGIN: SideNav-->
<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">
    <div class="brand-sidebar">
        <h1 class="logo-wrapper">
            <a class="brand-logo darken-1" href="{{ url('wholesaler') }}">
                <img class="hide-on-med-and-down" src="{{ asset('assets/images/logo/materialize-logo-color.png') }}"/>
                <img class="show-on-medium-and-down hide-on-med-and-up"
                     src="{{ asset('assets/images/logo/materialize-logo-color.png') }}"/>
                <span class="logo-text hide-on-med-and-down">{{ env('APP_NAME') }}</span>
            </a>
            <a class="navbar-toggler" href="#">
                <i class="material-icons">radio_button_checked</i>
            </a>
        </h1>
    </div>
    <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out"
        data-menu="menu-navigation" data-collapsible="menu-accordion">
        <li class="active bold">
            <a class=" waves-effect waves-cyan @if($data['menu'] == 'dashboard') active @endif"
               href="{{ route('wholesaler_dashboard') }}">
                <i class="material-icons">settings_input_svideo</i>
                <span class="menu-title" data-i18n="Dashboard">Dashboard</span>
                {{--                <span class="badge badge pill orange float-right mr-10">3</span>--}}
            </a>
        </li>
        <li class="bold">
            <a class="waves-effect waves-cyan @if($data['menu'] == 'enquiry') active @endif"
               href="{{ route('wholesaler_enquiry') }}">
                <i class="material-icons">message</i>
                <span class="menu-title" data-i18n="Mail">Enquiry</span>
                {{--                <span class="badge badge pill purple float-right mr-10">3</span>--}}
            </a>
        </li>
        <li class="bold">
            <a class="waves-effect waves-cyan @if($data['menu'] == 'products') active @endif"
               href="{{ route('wholesaler_product') }}">
                <i class="material-icons">shopping_cart</i>
                <span class="menu-title" data-i18n="Mail">Products</span>
                {{--                <span class="badge badge pill purple float-right mr-10">3</span>--}}
            </a>
        </li>
        <li class="bold">
            <a class="waves-effect waves-cyan @if($data['menu'] == 'profile') active @endif"
               href="{{ route('wholesaler_profile') }}">
                <i class="material-icons">person_outline</i>
                <span class="menu-title" data-i18n="User Profile">Profile</span>
            </a>
        </li>
        <li class="bold">
            <a class="waves-effect waves-cyan @if($data['menu'] == 'change_password') active @endif"
               href="{{ route('vendor_change_password') }}">
                <i class="material-icons">lock</i>
                <span class="menu-title" data-i18n="User Profile">Change Password</span>
            </a>
        </li>
{{--        <li class="bold">--}}
{{--            <a class="waves-effect waves-cyan @if($data['menu'] == 'commission') active @endif"--}}
{{--               href="{{ route('wholesaler_commission') }}">--}}
{{--                <i class="material-icons">money</i>--}}
{{--                <span class="menu-title" data-i18n="User Profile">Commissions</span>--}}
{{--            </a>--}}
{{--        </li>--}}
        <li class="bold">
            <a class="waves-effect waves-cyan @if($data['menu'] == 'notifications') active @endif"
               href="{{ route('wholesaler_logout') }}">
                <i class="material-icons">logout</i>
                <span class="menu-title" data-i18n="User Profile">Logout</span>
            </a>
        </li>
    </ul>
    <div class="navigation-background"></div>
    <a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only"
       href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
</aside>
<!-- END: SideNav-->

<!-- BEGIN: Page Main-->
@yield('content')
<!-- END: Page Main-->


<!-- BEGIN: Footer-->
<footer
    class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
    <div class="footer-copyright">
        <div class="container">
            <span>&copy; 2020
                <a href="" target="_blank">MESSAGE APP</a>
                All rights reserved.
            </span>
            <span class="right hide-on-small-only">Design and Developed by
                <a href="#">SUNCITY TECHNO</a>
            </span>
        </div>
    </div>
</footer>


<script src="{{asset('assets/js/plugins.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/search.min.js')}}"></script>
{{--<script src="{{asset('assets/js/scripts/dashboard-modern.js')}}" type="text/javascript"></script>--}}
{{--    <script src="{{asset('assets/js/scripts/intro.min.js')}}" type="text/javascript"></script>--}}
<script src="{{asset('assets/vendors/data-tables/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js')}}"
        type="text/javascript"></script>

<script src="{{asset('assets/js/custom/custom-script.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/scripts/customizer.min.js')}}" type="text/javascript"></script>

{{--<script src="{{asset('assets/js/scripts/page-users.min.js')}}" type="text/javascript"></script>--}}
<script src="{{asset('assets/vendors/jquery-validation/jquery.validate.min.js')}}" type="text/javascript"></script>


<script src="{{ asset('assets/vendors/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/vendors/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/js/scripts/form-select2.min.js') }}"></script>
<script src="{{ asset('assets/js/scripts/form-select2.min.js') }}"></script>


{{--    <script src="../../../app-assets/"></script>--}}
{{--    <script src="../../../app-assets/vendors/chartist-js/chartist-plugin-fill-donut.min.js"></script>--}}
<script>
    setTimeout(function () {
        $(".card-alert").slideUp(300);
    }, 5000);
</script>

</body>
</html>
