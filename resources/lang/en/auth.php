<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    // Add Wholesaler from Admin side
    'wholesaler_added' => 'Wholesaler account has been created successfully.',

    // Add Retailer from Admin Side
    'admin_retailer_added' => 'Retailer has been added successfully',

    // Add Product from Vendor Side
    'product_added' => 'Product has been added successfully'

];
